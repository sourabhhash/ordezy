<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantCategory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(RestaurantCategory::class, function (Faker $faker) {
    return [
        //
        "restaurant_id"=>$faker->numberBetween(1, App\Restaurant::count()),
        "title"=>$faker->randomElement(["Ethnic","Fast Food","Family Style","Casual Dining","Fast Casual"]),
        "slug"=>$faker->uuid
    ];
});
