<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantEmploye;
use Faker\Generator as Faker;

$factory->define(RestaurantEmploye::class, function (Faker $faker) {
    return [
        //
         "restaurant_id"=>$faker->numberBetween(1,App\Models\Restaurant::count()),
         "name"=>$faker->name,
         "email"=>$faker->safeEmail,
         "contact_number"=>$faker->phoneNumber,
         "role"=>$faker->randomElement(['admin', 'employee']),
         "password"=>"$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         "profile_img"=>"image",
         "gender"=>$faker->randomElement(['M','F']),
         "status"=>"1",
         "email_verified"=>"Y"
    ];
});
