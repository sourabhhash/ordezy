<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantSubcategory;
use Faker\Generator as Faker;

$factory->define(RestaurantSubcategory::class, function (Faker $faker) {
    return [
        //
       "category_id"=>$faker->numberBetween(1,App\RestaurantCategory::count()),
       "slug"=>$faker->uuid,
       "title"=>$faker->randomElement(["Doughnuts Shops","Buffet Restaurant","Hot Dog Restaurant", "Jew Restaurant","Heitian Restaurants","family dining"]) 
    ];
});
