<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantJob;
use Faker\Generator as Faker;

$factory->define(RestaurantJob::class, function (Faker $faker) {
    return [
        //
        "restaurant_id"=>$faker->numberBetween(1, App\Restaurant::count()),
        "title"=>$faker->randomElement(["Chef","Bartender","Dishwasher","Manager"]),
        "description"=>"job description",
        "experience"=>$faker->randomElement([1,2,3,4]),
        "timing"=>"8 hour",
        "skill"=>"skill",
        "budget"=>"1000"
    ];
});
