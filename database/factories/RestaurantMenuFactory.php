<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RestaurantMenu;
use Faker\Generator as Faker;

$factory->define(RestaurantMenu::class, function (Faker $faker) {
    return [
        //
       "restaurant_id"=>$faker->numberBetween(1, App\Restaurant::count()),
       "category_id"=>$faker->numberBetween(1,App\RestaurantCategory::count()),
       
    ];
});
