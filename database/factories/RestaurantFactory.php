<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Restaurant;
use Faker\Generator as Faker;

$factory->define(Restaurant::class, function (Faker $faker) {
    return [
        //
        "slug"=>$faker->uuid,
        "name"=>$faker->name,
        "email"=>$faker->safeEmail,
        "website"=>$faker->safeEmailDomain,
        "contact_number"=>$faker->phoneNumber,
        "location"=>$faker->address


    ];
});
