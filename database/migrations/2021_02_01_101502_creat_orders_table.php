<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('restaurant_id');
            $table->string('cart_id');
            $table->string('customer_id');
            $table->string('employee_id');
            $table->string('quantity');
            $table->string('total_amount');
            $table->string('shipping_cost')->default(0);
            $table->string('coupon_code');
            $table->string('payment_type');
            $table->string('total_calories');
            $table->string('delivering_instructions')->nullable();
            $table->string('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
