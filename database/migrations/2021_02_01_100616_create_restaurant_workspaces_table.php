<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantWorkspacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_workspaces', function (Blueprint $table) {
            $table->id();
            $table->string('restaurant_id');
            $table->string('domain')->nullable();
            $table->string('restaurant_logo');
            $table->string('restaurant_favicon')->nullable();
            $table->string('brand_color')->nullable();
            $table->string('profile_img');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_workspaces');
    }
}
