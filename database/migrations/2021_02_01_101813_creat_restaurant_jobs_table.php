<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatRestaurantJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('restaurant_id');
            $table->string('title');
            $table->string('description');
            $table->string('experience');
            $table->string('timing');
            $table->string('skill');
            $table->string('budget');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_jobs');
    }
}
