<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatCustomersChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers_chat', function (Blueprint $table) {
            $table->id();
            $table->string('chat_room_id');
            $table->string('sender_id');
            $table->string('sender_name');
            $table->string('message');
            $table->string('receiver_id');
            $table->string('seen_by_restaurant')->default(0);
            $table->string('seen_by_customer')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers_chat');
    }
}
