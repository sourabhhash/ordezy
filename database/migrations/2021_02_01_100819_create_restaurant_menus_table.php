<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_menus', function (Blueprint $table) {
            $table->id();
            $table->string('restaurant_id');
            $table->string('category_id');
            $table->string('subcategory_id');
            $table->string('title');
            $table->string('thumbnail_pic');
            $table->string('slug');
            $table->string('services_type');  //breakfast,lunch etc
            $table->string('description')->nullable();
            $table->string('calories');
            $table->string('price');
            $table->string('Ingredients')->nullable();
            $table->string('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_menus');
    }
}
