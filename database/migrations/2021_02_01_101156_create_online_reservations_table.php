<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlineReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_reservations', function (Blueprint $table) {
            $table->id();
            $table->string('restaurant_id');
            $table->string('full_name');
            $table->string('email');
            $table->string('phone');
            $table->string('number_of_guests');
            $table->string('reservation');  //date & time
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_reservations');
    }
}
