<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\Models\Restaurant::class, 50)->create();
//        factory(App\Models\RestaurantCategory::class, 100)->create();
        factory(App\Models\RestaurantEmploye::class, 100)->create();
//        factory(App\Models\RestaurantJob::class,100)->create();
//        factory(App\Models\RestaurantSubcategory::class, 100)->create();

    }
}
