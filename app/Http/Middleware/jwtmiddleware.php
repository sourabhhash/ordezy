<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth;
class jwtmiddleware
{
public function handle($request, Closure $next, $guard=null){ 
Auth::shouldUse('employee');
try{
if (! $user = JWTAuth::parseToken()->authenticate()){
return response([                  
'status'=>'auth_fail','code'   => 10991, 'response' => null ]);
}
}
catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){
// If the token is expired, then it will be refreshed and added to the headers
return response([                  
'status'=>'auth_fail','code'   => 10991, 'response' => null ]);
try{
$refreshed = JWTAuth::refresh(JWTAuth::getToken());
$user = JWTAuth::setToken($refreshed)->toUser();
header('Authorization: Bearer ' . $refreshed);
}
catch (\Tymon\JWTAuth\Exceptions\JWTException $e){
return response([                  
'status'=>'auth_fail',
'code'   => 10991, 
'response' => null ]);
}
}
catch (\Tymon\JWTAuth\Exceptions\JWTException $e){
return response([                  
'status'=>'auth_fail','code'   => 10991, 'response' => null ]);
}
// Login the user instance for global usage
//       Auth::login($user,false);
return  $next($request);
}
}

