<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth;
class customerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        Auth::shouldUse('customer');
        try
        {
            if (! $user = JWTAuth::parseToken()->authenticate() )
            {
                
                 return response()->json([
                   'status'=>"auth_fail",
                   'msg'=>"Please Login ",
                   'code'   => 10991, // means auth error in the api,
                 ]);
            }
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
        {
            // If the token is expired, then it will be refreshed and added to the headers
            try
            {
                $refreshed = JWTAuth::refresh(JWTAuth::getToken());
                $user = JWTAuth::setToken($refreshed)->toUser();
                header('Authorization: Bearer ' . $refreshed);
            }
            catch (\Tymon\JWTAuth\Exceptions\JWTException $e)
            {
                 return response()->json([
                   'code'   => 103, // means not refreshable 
                 ]);
            }
        }
        catch (\Tymon\JWTAuth\Exceptions\JWTException $e)
        {
            return response()->json([
                   'status'=>"auth_fail",
                   'code'   => 1010, // means auth error in the api,
                   'response' => null // nothing to show 
            ]);
        }

        // Login the user instance for global usage
      


         return  $next($request);
    }
}
