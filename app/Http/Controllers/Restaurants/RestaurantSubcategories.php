<?php

namespace App\Http\Controllers\Restaurants;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RestaurantSubcategory;
use Auth;
use App\Models\RestaurantCategory;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use App\Models\RestaurantMenu;
use App\Models\RestaurantWorkspace;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class RestaurantSubcategories extends Controller
{
public function all(Request $request){
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$query = RestaurantSubcategory::where('restaurant_id',Auth::user()->restaurant_id)
->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);    
}
public function create(Request $request){
$input=[
"parent_title"=>$request->title,
"description"=>$request->description,
"icon"=>$request->icon,];
$rule= [
"description"=>"required|max:255",
"icon"=>"required|image"
];
$message=[
"title.required"=>"Title is mandetory",
"description.required"=>"Description is mandetory",
];
if($request->category==0){
$rule=["parent_title"=>[Rule::unique('restaurant_subcategories')
->where('restaurant_id', auth()->user()->restaurant_id)]];         
}
else{
$input=[
"title"=>$request->title,
"description"=>$request->description,
"icon"=>$request->icon
];
$rule=[
"title"=>["required",
Rule::unique('restaurant_subcategories')->where('restaurant_id', auth()->user()->restaurant_id)
->where('title',$request->title)]];
}        
$validator=Validator::make($input, $rule,$message);
if($validator->fails()){
return response(["status"=>"validErr","data"=>$validator->errors()]);
}
try {
$restid=Auth::user()->restaurant_id;
if(isset($request->category) && ((int)$request->category) !== 0){
$cat=$request->category;
$category= RestaurantSubcategory::where('id',$cat)->first();
$cattitle=$category->parent_title;  
$title=$request->title;
}
else{
$cat=0;
$cattitle=$request->title;
$title=null;
}  
$sub= new RestaurantSubcategory();
$sub->restaurant_id=$restid;
$sub->parent_id= $cat;
$sub->parent_title=$cattitle;
$sub->title=$title;
$sub->slug=(string) Str::uuid();
$sub->description=$request->description;
$file=$request->file('icon');
$name=$file->getClientOriginalName();
$name=Carbon::now()->timestamp.'-'.$restid.'-'.$name;
$sub->icon=$name;
$count=$sub->save();
$file->move(public_path("/images/category/"),$name);
if($count>0){
return response(["status"=>"success","msg"=>"Record created successfully"],200);
}
else{
return response(["status"=>"faliure","msg"=>"Something went wrong"],200);        
}
}//try block end 
catch(Exception $e){
return response(["status"=>"faliure","msg"=>"OOPS Exception occured"],500);    
}    
}
public function edit(Request $request, $slug)
{
$data=RestaurantSubcategory::where('slug',$slug)->get();
if($data->count()>0)
{
return response(['status'=>"Success",
'data'=>$data,
'msg'=>"Record get succesfully"
]);
}
else
{
return response(['status'=>"Faliure",
'msg'=>"Record not found"
]);        
} 
}
public function update(Request $request){
$cat=RestaurantSubcategory::where('id',$request->id)->first();
$input=[
"icon"=>$request->icon,
"title"=>$request->title,
"description"=>$request->description,];
$rule=[
"description"=>"required",];
 if($request->category)
 {
$rule["title"]=['required',Rule::unique('restaurant_subcategories')->where('restaurant_id', auth()->user()->restaurant_id)->where( function($query)use($cat){

	return $query->where('title','!=',trim($cat->title));
})];
 }
 else
 {
$input=[
"icon"=>$request->icon,
"parent_title"=>$request->title,
"description"=>$request->description,]; 	
$rule["parent_title"]=['required',Rule::unique('restaurant_subcategories')
->where('restaurant_id', auth()->user()->restaurant_id)
->where( function($query)use($cat){

	return $query->where('parent_title','!=',trim($cat->parent_title));
})]; 	
 }
$message=[
"title.required"=>"Title is required.",
"description.required"=>"Description is required",];
$validator=Validator::make($input, $rule,$message);
if($request->file('icon')){
$rules['icon']='image';
}
if($validator->fails()){
return response(["status"=>"validErr","data"=>$validator->errors()]);
}
if($request->file('icon')){
$file=$request->file('icon');
$name=$file->getClientOriginalName();
$name= Carbon::now()->timestamp.'-'.$request->id.'-'.$name; 
$file->move(public_path("/images/category/"),$name);
if(\File::exists(public_path('/images/category/'.$request->imgicon))){
\File::delete(public_path('/images/category/'.$request->imgicon));
}       
}
else{
$name=$request->imgicon;
}
if($request->category==0){
$parent_title=$request->title;
$category=RestaurantSubcategory::where('id',$request->id)->first();
$count=RestaurantSubcategory::where('parent_title',$category->parent_title)->update(["parent_title"=>
	$parent_title]);
RestaurantSubcategory::where('id',$request->id)->update([
"parent_title"=>$request->title,
"icon"=>$name,
"description"=>$request->description
]);
}
else{
$category=RestaurantSubcategory::where('id',$request->category)->first();
$count=RestaurantSubcategory::where('id',$request->id)->update([
"title"=>$request->title,
"icon"=>$name,
"parent_id"=>$request->category,
"parent_title"=>$category->parent_title,
"description"=>$request->description]);
}
if($count>0){
return response(["status"=>"success","msg"=>"Record Updated Successfully"],200);
}
else{
return response(["status"=>"faliure","msg"=>"Something went wrong"],200);
}       
}
public function delete(Request $request){
$category=RestaurantSubcategory::where('id',$request->id)->first(); 
$parent_id=$category->parent_id;
if($parent_id==0){
$array=array();
$child=RestaurantSubcategory::where('parent_id',$category->id)->get();
foreach($child as $c){
if(\File::exists(public_path('/images/category/'.$c->icon))){
\File::delete(public_path('/images/category/'.$c->icon));
}   
}
$count=RestaurantSubcategory::where('parent_id',$category->id)->delete();
RestaurantMenu::where('category_id',$category->id)->delete();
}
$cat=RestaurantSubcategory::where('id',$request->id)->first();
if(\File::exists(public_path('/images/category/'.$cat->icon))){
\File::delete(public_path('/images/category/'.$cat->icon));
}
RestaurantMenu::where('subcategory_id',$request->id)->delete();      
$count=RestaurantSubcategory::where('id',$request->id)->delete();
//       $count=99;
if($count>0){
return response(["status"=>"success","msg"=>"Record deleted successfully"],200);
}
else{return response(["status"=>"faliure","msg"=>"Something went wrong"
],200);        
}
}
public function categoryOfRestaurant(){
$rest=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->first();
$restaurant=Restaurant::where('id',$rest->restaurant_id)->first();
$data=RestaurantMenu::where('restaurant_id',$rest->restaurant_id)->
select(DB::raw('count(parent_title)-1 as count'),'parent_title as name')
->groupBy('parent_title')->havingRaw('count > ?', [0])->get();  
return response(['data'=>$data, 'restaurant'=>$restaurant]);
}

}
