<?php

namespace App\Http\Controllers\Restaurants;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\OnlineReservation;
class Reservation extends Controller
{
    //
     public function all(Request $request)
     {
       $data=DB::table('online_reservations')
       ->join('restaurants','restaurants.id','=','online_reservations.restaurant_id')
       ->select('online_reservations.id','restaurants.name as restaurant','online_reservations.full_name as Name','online_reservations.phone',
    'online_reservations.number_of_guests')->paginate(5);
      return response(["status"=>"success",
                       "data"=>$data
                      ],200);
     }
     public function create(Request $request)
     {

       $input=["restaurant"=> $request->restaurant,
                "rname"=>$request->rname,
                "email"=>$request->email,
                "phone"=>$request->phone,
                "guest"=>$request->guest,
                "reservation"=>$request->reservation
              ];
        $rule=["restaurant"=>"required",
               "rname"=>"required",
               "email"=>"required|email",
               "phone"=>"required",
                "guest"=>"required",
               "reservation"=>"required"              ];
        $message=["restaurant.required"=>"restaurant is mandetory",
                  "rname.required"=>"Name is required",
                  "email.email"=>"email is mandetory",
                  "phone.required"=>"phone is mandetory",
                  "guest.required"=>" Number of guest is mandetory",
                  "reservation.required"=>"reservation is mandetory",
                ];
        $validator=Validator::make($input,$rule,$message);
        if ($validator->fails()) 
        {
            return response()->json($validator->errors());
        }
         $res= new OnlineReservation();        
         $res->restaurant_id= $request->restaurant;
                $res->full_name=$request->rname;
                $res->email=$request->email;
                $res->phone=$request->phone;
                $res->number_of_guests=$request->guest;
                $res->reservation=$request->reservation;
                $res->status=1;
                $count=$res->save();
                if($count>0)
                {
                   return response([
                      "status"=>"success",
                       "msg"=>"Record created successfully"
                                   ]);
                }
                else
                {
               return response([
                      "status"=>"faliure",
                       "msg"=>"Something went wrong"
                                   ]);
                }
     }
     public function edit(Request $request,$id)
     {

       $data=OnlineReservation::where('id',$id)->get();	    

          return response(["status"=>"success",
                           "data"=>$data
                          ]);
     }
     public function update(Request $request)
     {
     	       $input=["restaurant"=> $request->restaurant,
                "full_name"=>$request->full_name,
                "email"=>$request->email,
                "phone"=>$request->phone,
                "number_of_guests"=>$request->number_of_guests,
                "reservation"=>$request->reservation
              ];
        $rule=["restaurant"=>"required",
               "full_name"=>"required",
               "email"=>"required|email",
               "phone"=>"required",
                "number_of_guests"=>"required",
               "reservation"=>"required"              ];
        $message=["restaurant.required"=>"restaurant is mandetory",
                  "full_name.required"=>"Name is required",
                  "email.email"=>"email is mandetory",
                  "phone.required"=>"phone is mandetory",
                  "number_of_guests.required"=>" Number of guest is mandetory",
                  "reservation.required"=>"reservation is mandetory",
                ];
        $validator=Validator::make($input,$rule,$message);
        if ($validator->fails()) 
        {
            return response()->json($validator->errors());
        }
      $count=OnlineReservation::where('id',$request->id)->update([
              "restaurant_id"=> $request->restaurant,
                "full_name"=>$request->full_name,
                "email"=>$request->email,
                "phone"=>$request->phone,
                "number_of_guests"=>$request->number_of_guests,
                "reservation"=>$request->reservation
              ]);
      if($count>0)
                {
                   return response([
                      "status"=>"success",
                       "msg"=>"Record updated successfully"
                                   ]);
                }
                else
                {
               return response([
                      "status"=>"faliure",
                       "msg"=>"Something went wrong"
                                   ]);
                }
     }
     public function delete(Request $request)
     {
      $count= OnlineReservation::where('id',$request->id)->delete();
       if($count>0)
       {
         return response(["status"=>"success",
                          "msg"=>"Record deleted sucessfully"]);
       }
       else
       {
         return response(["status"=>"faliure",
                          "msg"=>"Something went wrong"]);       	
       }
     }

}
