<?php
namespace App\Http\Controllers\Restaurants;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Restaurant;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\RestaurantMenu;
use App\Models\RestaurantCategory;
use Illuminate\Support\Str;
use DB;
use Carbon\Carbon;
use App\Models\RestaurantWorkspace;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class RestaurantMenus extends Controller{
public function all(Request $request) { 
$restaurant=Auth::guard('employee')->user()->restaurant_id;
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$query = RestaurantMenu::where('restaurant_id',$restaurant)->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);
}
public function create(Request $request){
$input=[
"category"=>$request->category,
"subcategory"=>$request->subcategory,
"title"=>$request->title,
"thumbnail"=>$request->thumbnail,
"service"=>$request->service,
"description"=>$request->description,
"calories"=>$request->calories,
"price"=>$request->price,
"ingredient"=>$request->ingredient,
"quantity"=>$request->quantity];
$rule=[
"category"=>"required",
"title"=>['required',Rule::unique('restaurant_menus')->where('restaurant_id',Auth::user()->restaurant_id)],//"required|unique:restaurant_menus,title",
"thumbnail"=>"required|image",
"service"=>"required",
"description"=>"required",
"calories"=>"required|numeric",
"price"=>"required|numeric",
"ingredient"=>"required",
//"quantity"=>"required"
];
$message=[
"category.required"=>"category is required",
"title.required"=>"title is required",
"title.unique"=>"Menu title  already exist",
"thumbnail.required"=>"thumbnail is required",
"service.required"=>"service is required",
"description.required"=>"description is required",
"calories.required"=>"caleories is required",
"price.required"=>"price is required",
"ingredient.required"=>"ingredient is required",
//"quantity.required"=>"quantity is required",
];
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()){
return response(["status"=>"validErr","data"=>$validator->errors()]);
}
$menu= new RestaurantMenu();
$menu->restaurant_id=Auth::user()->restaurant_id;
$menu->category_id=$request->category;
$menu->subcategory_id=$request->subcategory ? $request->subcategory:0 ;
$menu->title=$request->title;
$menu->services_type=$request->service;
$menu->description=$request->description;
$file=$request->file('thumbnail');
$name=$file->getClientOriginalName();
$name=Carbon::now()->timestamp.'-'.Auth::user()->restaurant_id.'-'.$name;
$menu->thumbnail_pic=$name;
$menu->slug= Str::slug($request->title.'-'.$request->restaurant);
$menu->calories=$request->calories;
$menu->price=$request->price;
$menu->ingredients=$request->ingredient;
$menu->quantity=0;//$request->quantity;
$menu->recomended=$request->recomended ? $request->recomended :0;
$menu->manage_stock=$request->stock ? $request->stock :0;
$menu->stock_quantity=$request->stock_quantity ? $request->stock_quantity :0;
$count=$menu->save();
$file->move(public_path("/images/Restaurant-Menus/"),$name);
if($count>0){
return response(["status"=>"success","msg"=>"Menu Created Successfully"]);
}
else{return response(["status">"faliure","msg"=>"Something went wrong"]);        	
}
}
public function edit(Request $request, $slug){
$editmenu=RestaurantMenu::where('slug',$slug)->get(); 
return response(['status'=>"success",'data'=>$editmenu,'msg'=>"success"],200);
}
public function update(Request $request){
$input=[
"category"=>$request->category,

"title"=>$request->title,
"service"=>$request->service,
"description"=>$request->description,
"calories"=>$request->calories,
"price"=>$request->price,
"ingredient"=>$request->ingredient,
//"quantity"=>$request->quantity,
"thumbnail_pic"=>$request->thumbnail,];
$rule=[
"category"=>"required",

"title"=>['required',Rule::unique('restaurant_menus')->where('restaurant_id',Auth::user()->restaurant_id)
->where('id','<>',$request->id)],//"required|unique:restaurant_menus,title,".$request->id,
"service"=>"required",
"description"=>"required",
"calories"=>"required",
"price"=>"required|numeric",
"ingredient"=>"required",
//"quantity"=>"required"
];
$message=[
"category.required"=>"category is required",

"title.required"=>"title is required",
"service.required"=>"service is required",
"description.required"=>"description is required",
"calories.required"=>"caleories is required",
"price.required"=>"price is required",
"ingredient.required"=>"ingredient is required",
//"quantity.required"=>"quantity is required",
];
if($request->hasFile('thumbnail')){$rules['thumbnail_pic']='image';
}                  
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()){
return response(["status"=>"validErr","data"=>$validator->errors()]);
}
if($request->hasFile('thumbnail')){
$file=$request->file('thumbnail');
$name=$file->getClientOriginalName();
$name=Carbon::now()->timestamp.'-'.Auth::user()->restaurant_id.'-'.$name;
if(\File::exists(public_path('/images/Restaurant-Menus/').$request->thumb_name)){
\File::delete(public_path('/images/Restaurant-Menus/').$request->thumb_name);
}          
$file->move(public_path("/images/Restaurant-Menus/"),$name);
}
else{
$name=$request->thumb_name;
}
$count=RestaurantMenu::where('id',$request->id)->update([
"restaurant_id"=> Auth::user()->restaurant_id,
"category_id"=>$request->category,
"subcategory_id"=>$request->subcategory,
"title"=>$request->title,
"thumbnail_pic"=>$name,
"services_type"=>$request->service,
"description"=>$request->description,
"calories"=>$request->calories,
"price"=>$request->price,
"ingredients"=>$request->ingredient,
"quantity"=>0,//$request->quantity,
"manage_stock"=>$request->stock,
"recomended"=>$request->recomended ? $request->recomended: 0 ,
"stock_quantity"=>$request->stock_quantity,                               
]);
if($count>0){
return response(['status'=>"success",'msg'=>"Record updated successfully"]);
}
else{
return response(['status'=>"faliure",'msg'=>"Something went wrong"]);            	
}          
}
public function delete(Request $request){      
$count=RestaurantMenu::where('id',$request->id)->delete();
if($count>0){
if(\File::exists(public_path('/images/Restaurant-Menus/'.$request->image))){
\File::delete(public_path('/images/Restaurant-Menus/'.$request->image));
} 
return response(["status"=>"success","msg"=>"Record deleted Successfully"],200);
}
else{
return response(["status"=>"faliue","msg"=>"Something went wrong"],200);      	
}
}
public function menuOfRestaurant(Request  $request){
    
$restaurant=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->first();
$restaurant_id=$restaurant->restaurant_id;
$data=RestaurantMenu::whereHas('category', function ($q) use ($restaurant_id) {
$q->where('restaurant_id',$restaurant_id);
})->with('category')->get();
$category = $data->countBy(function ($item){
return $item['category']['parent_title'];
});

return response(['status'=>'success','data'=>$data,'category'=>$category,"restaurant"=>$restaurant]);
}


public function reconmendedMenuOfRestaurant(){
$restaurant=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->first();
$rest=Restaurant::where('id',$restaurant->restaurant_id)->first();
$data= DB::select('select id, title ,price,description,thumbnail_pic,stock_quantity,manage_stock, (select count(*) as recomended from restaurant_menus where 
recomended=1 and  category_id in 
(select distinct(parent_id) from restaurant_subcategories where restaurant_id=?)) as count from  restaurant_menus where recomended=1 and restaurant_id=?',[$restaurant->restaurant_id,$restaurant->restaurant_id]);

return response(['data'=>$data,"restaurant"=>$rest]);
}


}
