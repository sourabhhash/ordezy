<?php

namespace App\Http\Controllers\Restaurants;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Coupon as mycoupon;
use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Illuminate\Support\Str;
class Coupon extends Controller{

public function all(Request $request){
$restaurant=Auth::guard('employee')->user()->restaurant_id;
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$query = mycoupon::where('restaurant_id',$restaurant)->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);
}
public function create(Request $request){   
$input=['title'=> $request->title,
'description'=>$request->description,
'code'=>$request->code,
'type'=>$request->type,
'price'=>$request->price,
'days' =>$request->days
];
$rule=['title'=>['required',Rule::unique('coupons')->where('restaurant_id',Auth::user()->restaurant_id)],
'description'=>'required',
'code'=>['required',Rule::unique('coupons')->where('restaurant_id',Auth::user()->restaurant_id)],
'type'=>'required',
'price'=>'required',
'days'=>'required'];
$message=['title.required'=>'Coupon title is required.',
'description.required'=>'Coupen description is required',
'code.required'=>'Coupen code is required',
'type.required'=>'Coupen type is required',
'price.required'=>'Coupen amount is required',
'days.required'=>'Days for which coupon is valid , is required'
];
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()){
return response(["status"=>"validErr",
"data"=>$validator->errors()]);
}
$coupon = new mycoupon();
$coupon->restaurant_id= Auth::user()->restaurant_id;
$coupon->title=$request->title;
$coupon->description=$request->description;
$coupon->price=$request->price;
$coupon->slug=Str::uuid();
$coupon->period=$request->days;
$coupon->code=$request->code;
$coupon->type=$request->type;
$count=$coupon->save();
if($count>0){return response(['status'=>'success','msg'=>'Coupen created successfully']);
}
else{return response(['status'=>'faliure','msg'=>'Something went wrong']);                    
}

}
public function edit(Request $request, $slug ){
$data=mycoupon::where('slug',$slug)->get();
return response(['status'=>'success','data'=>$data]);
}
public function update(Request $request){
$input=['title'=> $request->title,
'description'=>$request->description,
'code'=>$request->code,
'type'=>$request->type,
'price'=>$request->price,
'days' =>$request->days
];
$rule=['title'=>['required',Rule::unique('coupons')->where('restaurant_id',Auth::user()->restaurant_id)
->where('id','<>',$request->id)],
'description'=>'required',
'code'=>['required',Rule::unique('coupons')->where('restaurant_id',Auth::user()->restaurant_id)
->where('id','<>',$request->id)],
'type'=>'required',
'price'=>'required',
'days'=>'required'
];
$message=['title.required'=>'Coupon title is required.',
'description.required'=>'Coupen description is required',
'code.required'=>'Coupen code is required',
'type.required'=>'Coupen type is required',
'price.required'=>'Coupen amount is required',
'days.required'=>'Days for which coupon is valid , is required'
];
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()){
return response(["status"=>"validErr","data"=>$validator->errors()]);
}
$count=mycoupon::where('id',$request->id)->update([
"title"=>$request->title,
"description"=>$request->description,
"price"=>$request->price,
"period"=>$request->days,
"code"=>$request->code,
"type"=>$request->type,
]);
if($count>0){
return response(['status'=>'success','message'=>'Coupon updated successfully'],200);
}
else{
return response(['status'=>'success','message'=>'Coupon updated successfully'],200);          
}
}
public function delete(Request $request){
$count=mycoupon::where('slug',$request->slug)->delete();
if($count>0){
return response(['status'=>'success','msg'=>'Coupon deleted successfully']);
}
else{
return response(['status'=>'faliure','msg'=>'Something went wrong']);
}
}
}
