<?php

namespace App\Http\Controllers\Restaurants;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RestaurantEmploye;
use Illuminate\Support\Facades\Validator;
use Auth;
use JWTFactory;
use JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class RestaurantEmployees extends Controller
{

public function login(Request $request){
$validator = Validator::make($request->all(), [
'email' => 'required|string|email|max:255',
'password'=> 'required']);
if ($validator->fails()) {return response()->json($validator->errors());}
$credentials = $request->only('email', 'password');
$token=null;
if($token=Auth::guard('employee')->attempt($credentials)){
return response(["status"=>"success","token"=>$token,"user"=>'employee'],200);
} 
return response(["status"=>"auth_fail"]);       
}
public function all(Request $request){
$restaurant=Auth::guard('employee')->user()->restaurant_id;
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$query = RestaurantEmploye::where('restaurant_id',$restaurant)
->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);
}
public function create(Request $request){  
try{
$input=[
"name"=>$request->name,
"email"=>$request->email,
"contact"=>$request->contact,
"role"=>$request->role,
"password"=>$request->password,
"password_confirmation"=>$request->password_confirmation,
"gender"=>$request->gender,
"country"=>$request->country,
"image"=>$request->image
];
$rules=[
"name"=>"required",
"email"=>"required|email|unique:restaurant_employes,email",
"contact"=>"required|numeric|unique:restaurant_employes,contact_number",
"role"=>"required",
"password"=>"required|confirmed",
"gender"=>"required",
"image"=>"required|image",
];
$messages=[
"name.required"=>"Name is mandetory.",
"email.required"=>"Email is mandetory.",
"email.email"=>"Email formait is not valid.",
"email.unique"=>"Email already exists.",
"contact.required"=>"Contact number is mandetory.",
"role.required"=>"Role is mandetory",
"password.required"=>"Password is mandetory",
"gender.required"=>"Gender is mandetory",
"image.required"=>"Please upload image"
];    
$validator=Validator::make($input,$rules,$messages);                   
if($validator->fails()){return response(['status'=>'validErr','data'=>$validator->errors()]);
}
$file=$request->file('image');
$name=$file->getClientOriginalName();
$name=Carbon::now()->timestamp.'-'.Auth::user()->restaurant_id.'-'.$name;
$name=$name;
$emp= new RestaurantEmploye();
$emp->restaurant_id=Auth::user()->restaurant_id;
$emp->name=$request->name;
$emp->email=$request->email;
$emp->contact_number=$request->contact;
$emp->role=$request->role;
$emp->password=Hash::make($request->password);
$emp->uuid= (string) Str::uuid();
$emp->gender=$request->gender;
$emp->country=$request->country;
$emp->city=$request->city;
$emp->state=$request->state;
$emp->street=$request->street;
$emp->zipcode=$request->zip;
$emp->email_verified='N';
$emp->profile_img=$name;
$count=$emp->save();
$file->move(public_path("/images/employees/"),$name);
if($count>0){return response(["status"=>"success","msg"=>"Employee created successfully."],200);
}
else{return response(["status"=>"Faliure","msg"=>"Something went wrong."],200);    
}
}
catch(Exception $ex){
abort(500);
}
}
public function edit(Request $request,$uuid){
$emp= RestaurantEmploye::where('uuid',$uuid)->get();
if(count($emp)>0){return response(['status'=>"success",'data'=>$emp[0],'msg'=>'record get successfully']);
}
else{
return response(['status'=>"faliure",'msg'=>"Something went wrong"]);           
}
}
public function update(Request $request){
$input=[
"name"=>$request->name,
"email"=>$request->email,
"contact"=>$request->contact,
"role"=>$request->role,
"password"=>$request->password,
"password_confirmation"=>$request->password_confirmation,
"gender"=>$request->gender,
"country"=>$request->country,
"image"=>$request->image];
$rules=[
"name"=>"required",
"email"=>"required|email|unique:restaurant_employes,email,".$request->id,
"contact"=>"required|numeric|unique:restaurant_employes,contact_number,".$request->id,
"role"=>"required",
"gender"=>"required",];
$messages=[
"name.required"=>"Name is mandetory.",
"email.required"=>"Email is mandetory.",
"email.email"=>"Email formait is not valid.",
"email.unique"=>"Email already exists.",
"contact.required"=>"Contact number is mandetory.",
"role.required"=>"Role is mandetory",
"gender.required"=>"Gender is mandetory",];    
if($request->image){$rules["image"]="required|image";
}       
$validator=Validator::make($input,$rules,$messages);                   
if($validator->fails()){return response(['status'=>'validErr','data'=>$validator->errors()]);
}
if($request->file('image')){
$file=$request->file('image');
$name=$file->getClientOriginalName();
$name= Carbon::now()->timestamp.'-'.Auth::user()->restaurant_id.'-'.$name;  
$file->move(public_path("/images/employees/"),$name);
if(\File::exists(public_path('/images/employees/').$request->img)){
\File::delete(public_path('/images/employees/').$request->img);
}   
}
else{
$name =$request->img;
}
$count=RestaurantEmploye::where('id',$request->id)->update([
"restaurant_id"=>Auth::user()->restaurant_id,
"name"=>$request->name,
"email"=>$request->email,
"contact_number"=>$request->contact,
"role"=>$request->role,
"gender"=>$request->gender,
"country"=>$request->country,
"city"=>$request->city,
"state"=>$request->state,
"street"=>$request->street,
"zipcode"=>$request->zip,
"email_verified"=>'N',
"profile_img"=>$name]);
if($count>0){return response(["status"=>"success","msg"=>"Record updated successfully."]);
}
else{
return response(["status"=>"faliure","msg"=>"Something went wrong."]);           
}
}
public function delete(Request $request){
$count=RestaurantEmploye::where('uuid',$request->uuid)->delete();
if($count>0){
return response(['status'=>"success",'msg'=>'Employee deleted successfully']);
}
else{
return response(['status'=>"faliure",'msg'=>'Something went wrong']);            
} 
}
}
