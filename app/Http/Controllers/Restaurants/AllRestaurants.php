<?php

namespace App\Http\Controllers\Restaurants;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Restaurant;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;
use App\Models\RestaurantMenu;
use DB;
use App\Models\RestaurantCategory;
use App\Models\RestaurantSubcategory;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class AllRestaurants extends Controller
{
    //

     public function all(Request $request)
      {
        $length = $request->input('length');
        $sortBy = $request->input('column');
        $orderBy = $request->input('dir');
        $searchValue = $request->input('search');
       $query = Restaurant::eloquentQuery($sortBy, $orderBy, $searchValue);
               $data = $query->paginate($length);
        
        return new DataTableCollectionResource($data);
        }

     public function filter(Request $request)
     {
      $parameter= $request->all();
        $name=$parameter['name'];
        $data=Restaurant::where('name', 'like', '%$name')->paginate(10);  
        return response(["status"=>"success",
                         "data"=>$data
                         ]);
     }   
      public function all_restaurants()
      {

        try
        {  
        $restaurant= new Restaurant();
        $all=$restaurant::all();
        if($all->count()>0)
        {
        return response([
        'status' => 'success',
        'data' => $all,
        'msg' => 'States list retrived successfully.'
                        ],200);
        }
        else
        {
        return response([
        "status"=>"Success",
        "data"=>"No data found",
        "msg"=>"No Restaurant"
                        ],200);
        }
        }
        catch(Exception $ex)
        {
           return  response(["status"=>"Faliure",
                             "msg"=>"Something went wrong"],500);
        }
       }
    public function create(Request $request)
    {
        $input=["name"=> $request->name,
                "email"=>$request->email,
                "contact"=>$request->contact,
                "website"=>$request->website
              ];
        $rule=["name"=>"required",
               "email"=>"required|email",
               "contact"=>"required",
               "website"=>"required"
              ];
        $message=["name.required"=>"Name is mandetory",
                  "email.required"=>"Email is required",
                  "email.email"=>"Email format is wrong",
                  "contact.required"=>"Contact Number is mandetory",
                  "website.required"=>"Website is mandetory" 
                ];
        $validator=Validator::make($input,$rule,$message);
        if ($validator->fails()) 
        {
            return response()->json($validator->errors());
        }        

       try{
       $restaurant= new Restaurant();
       $restaurant->slug=(string) Str::uuid();
       $restaurant->name=$request->name;
       $restaurant->email=$request->email;
       $restaurant->website=$request->website;
       $restaurant->contact_number=$request->contact;
       $restaurant->location="Location";
       $count=$restaurant->save();
       if($count>0)
       {
         return response([
         "status"=>"success",
         "msg"=>"Restaurant Created Successfully"
                          ],200);
       }
       else
       {
        return response([
        "status"=>"success",
        "msg"=>"Something Went wrong"
                        ],200);   
       }
    }
    catch(Exception $ex)
    {
       return  response(["status"=>"Faliure",
                         "msg"=>"Something went wrong"
                        ],500);
    }
    }
    public function edit(Request $request,$slug)
    {

       $restaurant= new Restaurant();
       $data=$restaurant::where("slug",$slug)->get();
       if($data)
       {
          return $data;
       }
       else
       {
         return response(["msg"=>"Restaurant Not Found"],200);
       }
    }
   public function update(Request $request)
   {
        $input=["name"=> $request->name,
                "email"=>$request->email,
                "contact"=>$request->contact,
                "website"=>$request->website
              ];
        $rule=["name"=>"required",
               "email"=>"required|email",
               "contact"=>"required",
               "website"=>"required"
              ];
        $message=["name.required"=>"Name is mandetory",
                  "email.required"=>"Email is required",
                  "email.email"=>"Email format is wrong",
                  "contact.required"=>"Contact Number is mandetory",
                  "website.required"=>"Website is mandetory" 
                ];
        $validator=Validator::make($input,$rule,$message);
        if ($validator->fails()) 
        {
            return response()->json($validator->errors());
        }        

      $restaurants= new Restaurant();
      $count=$restaurants::where('slug',$request->slug)
                   ->update(["name"=>$request->name,
                             "email"=>$request->email,
                             "website"=>$request->website,
                             "contact_number"=>$request->contact
                            ]);

      if($count>0)
      {
         return response(["status"=>"Success",
                          "msg"=>"Record Updated Successfully"
                         ],200);
      }
      else
      {
         return response(["status"=>"Success",
                          "msg"=>"Something went wrong"
                         ],200);


      } 

   } 
   public function delete(Request $request)
   {
      try
      {
     $count=Restaurant::where("email",$request->email)->delete();
     if($count>0)
     {
       return response(["status"=>"Success",
                        "msg"=>"Record deleted successfully"],200);
     }
     else
     {
       return response(["statu"=>"Success",
                        "msg"=>"Something went wrong"],200);      
     }
      }
      catch(Exception $ex)
      {
           return  response(["status"=>"Faliure",
                             "msg"=>"Something went wrong"],500);
      }
 

   }
/****************Get Restaurants Of Logined Owner************************/
   public function getRestaurant(Request $request)
   {    

      $restaurant= Auth::guard('employee')->user()->restaurant_id;
      $restaurant=Restaurant::where('id',$restaurant)->get();
      return response(["data"=>$restaurant],200);
   }
/********************************************************************************/   
/***********Get All Restaurants List******************************/
   public function getAllRestaurants()
   {
     $data= Restaurant::select('id','name')->get();  
     return response(["status"=>"success",
                      "data"=>$data]);
   }
/****************************************************************/   
/**************Get All Menus of All Restaurants of a owner******************/ 
   public function getMenu(Request $request)
   {
     $restaurant=Auth::guard('employee')->user()->restaurant_id;
      $data=Restaurant::find($restaurant)->RestaurantMenu()->get();
       return response(["status"=>"succes",
                        "data"=>$data,
                        "msg"=>"Record get successfully"
                      ]);
   }
/********************************************************************/   
/******************Get All Categories Of All Restaurants of a Owner***************/
   public function getCategory()
   {

      $restid=Auth::guard('employee')->user()->restaurant_id;
      $category=RestaurantSubcategory::where('restaurant_id',$restid)
      ->where('parent_id',0)->get();      
         return response(["status"=>"succes",
                         "data"=>$category,
                         "msg"=>"Record get successfully"
                       ]);
   }
/********************************************************************************/   
  public function getSubcategory(Request $request,$id)
  {

   $restid=Auth::guard('employee')->user()->restaurant_id;
   $subcategory=RestaurantSubcategory::where('restaurant_id',$restid)
   ->where('parent_id',$id)->get();
   return response(["status"=>"succes",
                      "data"=>$subcategory,
                      "msg"=>"Record get successfully"
                       ]);
  }

/*********************************************************************** */

/**************************Get Categories of Pertucular Restaurant****************/
   public function getMenuByRestaurant(Request $request,$id)
   {
      $data=DB::select('select restaurant_menus.id , restaurant_menus.slug, restaurant_menus.thumbnail_pic ,restaurant_menus.restaurant_id, restaurant_menus.category_id,
        restaurant_menus.restaurant_id,restaurant_menus.price,
        restaurant_categories.title as category, restaurant_subcategories.title as subcategory
        from  restaurants 
 inner join restaurant_categories on restaurant_categories.restaurant_id=  restaurants.id  
 inner join restaurant_subcategories on restaurant_subcategories.category_id= restaurant_categories.id
 inner join restaurant_menus on  restaurant_menus.restaurant_id = restaurants.id
 where restaurant_menus.category_id=restaurant_categories.id
 and restaurant_menus.subcategory_id=restaurant_subcategories.id
 and restaurants.id=?',[$id]);
     


     return response(["status"=>"success",
                      "data"=>$data,
                      "msg"=>"Success"
                    ]);
   }
/*************************************************************************/
}
