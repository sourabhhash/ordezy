<?php

namespace App\Http\Controllers\Restaurants;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Order;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use App\Models\OrderChef;
use App\Models\RestaurantEmploye;
class OrderController extends Controller
{
    //
public function all(Request $request){
$restaurant=Auth::guard('employee')->user()->restaurant_id;
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$query = Order::where('restaurant_id',$restaurant)->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);
}
public function OrderOChef(Request $request)
{
//  Order
}
public function OrderItmes(Request $request){ 
$chef=RestaurantEmploye::where('restaurant_id',Auth::user()->restaurant_id)->where('role','chef')->get();  
$data=Order::where('restaurant_id',Auth::user()->restaurant_id)->with('itemsOfOrder', 'customer')->get();
return response(['data'=>$data,'chef'=>$chef]);
}
}
