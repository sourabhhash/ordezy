<?php

namespace App\Http\Controllers\Restaurants;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RestaurantWorkspace;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File; 
use Auth;
use App\Models\Restaurant;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class Workspace extends Controller{
public function edit(Request $request){
$data= RestaurantWorkspace::where('restaurant_id',Auth::user()->restaurant_id)->get();
$restaurant=Restaurant::where('owner_id',Auth::user()->id)->get(); 
return response(['status'=>"success",'data'=>$data,'restaurant'=>$restaurant]);
}
public function update(Request $request){
$work= RestaurantWorkspace::where('restaurant_id',$request->restid)->get();
if($work->count()>0){
$work=$work[0];
$wid=$work->id;
}
else{$wid=0;
}
$input=["name"=> $request->rname,
"email"=>$request->email,
"website"=>$request->website,
"contact"=>$request->contact,
"domain"=>$request->domain,
"contact"=>$request->contact];
$rule=["name"=>"required|unique:restaurants,name,".$request->restid,
"email"=>"required|unique:restaurants,email,".$request->restid,
"website"=>"required|unique:restaurants,website,".$request->restid,
"contact"=>"required|unique:restaurants,contact_number,".$request->restid,
"domain"=>"required|unique:restaurant_workspaces,domain,".$wid,
"contact"=>"required|numeric|unique:restaurants,contact_number,".$request->restid];
$message=["name.required"=>"Restaurant name is required.",
"name.unique"=>"Restaurant name already exist.",
"email.required"=>"Restaurant email Required.",
"email.unique"=>"Restaurant email already required.",
"domain.required"=>"Restaurant domain is required.",
"domain.unique"=>"Domain Name already exists.",
"contact.required"=>"Contact number of restaurant required",
"contact.unique"=>"Contact number of restaurant already exists."];
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()){
return response(["status"=>"validErr","data"=>$validator->errors()]);
}
$up=Restaurant::where('id',$request->restid)->update([
"name"=>$request->rname,
"email"=>$request->email,
"website"=>$request->website,
"contact_number"=>$request->contact,
"manage_stock"=>$request->stock]);
$icon=$request->file('icon');
$logo=$request->file('logo');
if($icon){
$iname=$icon->getClientOriginalName();
$iname=Carbon::now()->timestamp.'_'.$iname;
\File::delete(public_path('/images/restaurant_workspace/icon/'.$request->wicon));
$icon->move(public_path('/images/restaurant_workspace/icon'),$iname);
}
else{$iname=$request->wicon;
}
if($logo){
$lname=$logo->getClientOriginalName();
$lname=Carbon::now()->timestamp.'_'.$lname;
\File::delete(public_path('/images/restaurant_workspace/logo/'.$request->wlogo));
$logo->move(public_path('/images/restaurant_workspace/logo'),$lname);
}
else{
$lname=$request->wlogo;
}
if($work->count()>0){
if($request->colour){
$clr=$request->colour;
}
else{
$clr=$request->clr; 
}
$count=RestaurantWorkspace::where('id',$work->id)->update([
"domain"=>$request->domain,
"brand_color"=>$clr,
"restaurant_favicon"=>$iname,
"restaurant_logo"=>$lname,]);
}
else{
$rest=  new RestaurantWorkspace();
$rest->restaurant_id=$request->restid;
$rest->domain=$request->domain;
$rest->brand_color=$request->colour;
$rest->restaurant_favicon=$iname;
$rest->restaurant_logo=$lname;
$count1=$rest->save();       
}
if((isset($count) && $count>0) || (isset($count1) && $count1>0)){
return response(["status"=>"success",
"msg"=>"Record updated successfully"],200);
}
else{return response(["status"=>"faliure","msg"=>"Something went wrong"],200);         	
} 
}
public function delete(Request $request){
$data=RestaurantWorkspace::where('id',$request->id)->get();
$count=RestaurantWorkspace::where('id',$request->id)->delete(); 
\File::delete(public_path($data[0]->restaurant_logo));
\File::delete(public_path($data[0]->restaurant_favicon));
if($count>0){
return response(["status"=>"success","msg"=>"record deleted successfully"],200);
}
else{
return response(["status"=>"faliure","msg"=>"Something went wrong"],200);       	
}
}
}
