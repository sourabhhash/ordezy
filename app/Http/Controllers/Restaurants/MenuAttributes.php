<?php
namespace App\Http\Controllers\Restaurants;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Models\RestaurantMenu;
use App\Models\MenuItemAttribute; 
use Illuminate\Support\Str;
use App\Models\Menu;
use App\Models\Restaurant;
use Auth;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Illuminate\Validation\Rule;
class MenuAttributes extends Controller
{

public function all(Request $request){
$restaurant=Auth::guard('employee')->user()->restaurant_id;
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$query=MenuItemAttribute::with(['menu'=>function($query){
$query->where('restaurant_id','=',Auth::user()->restaurant_id)->select('id','title');
}])->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);
}
public function create(Request $request){
$input=["menu"=> $request->menu,
"title"=>$request->title,
"price"=>$request->price,
"description"=>$request->description];
$rule=["menu"=>"required",
"price"=>"required|numeric",
"description"=>"required",
"title"=>["required", Rule::unique('menu_item_attributes')->where('menu_id', $request->menu)]];
$message=["menu.required"=>"Menu is mandetory",
"title.required"=>"Title is required",
"price.email"=>"Price is mandetory",
"description.required"=>"Description is mandetory",];
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()) {return response(['status'=>'validErr','data'=>$validator->errors()]);
}
$menu= new MenuItemAttribute();
$menu->menu_id=$request->menu;
$menu->title=$request->title;
$menu->slug= Str::slug($request->title.'-'.Auth::user()->restaurant_id);
$menu->price=$request->price;
$menu->description=$request->description;
$count=$menu->save();
if($count>0){
return response(["status"=>"success","msg"=>"Record created successfully"],200);
}
else{
return response(["status"=>"faliure","msg"=>"Something went wrong"],200);          	
}	
}
public function edit(Request $request,$slug){
$data=MenuItemAttribute::where('slug',$slug)->get();
return response(["data"=>$data]);
}
public function update(Request $request){
$input=["menu"=> $request->menu,
"title"=>$request->title,
"price"=>$request->price,
"description"=>$request->description];
$rule=["menu"=>"required",
"price"=>"required|numeric",
"description"=>"required",
"title"=>["required",Rule::unique('menu_item_attributes')
->where('menu_id', $request->menu)
->where('id','<>',$request->id)]];
$message=["menu.required"=>"Menu is mandetory",
"title.required"=>"Title is required",
"price.email"=>"Price is mandetory",
"description.required"=>"Description is mandetory",];
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()){return response()->json($validator->errors());}
$count=MenuItemAttribute::where('id',$request->id)->update(
["menu_id"=>$request->menu,
"title"=>$request->title,
"price"=>$request->price,
"description"=>$request->description
]);
if($count>0){return response(["status"=>"success","msg"=>"Record updated successfully"],200);
}
else{return response(["status"=>"faliure","msg"=>"Something went wrong"],200);        	
}          
}
public function delete(Request $request){
$count=MenuItemAttribute::where('id',$request->id)->delete();
if($count>0){
return response(["status"=>"success","msg"=>"Record deleted successfully"],200);
}
else{
return response(["status"=>"faliure","msg"=>"Something went wrong"],200);      	
}
}
}
