<?php

namespace App\Http\Controllers\Restaurants;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\RestaurantJob;
use DB;
use Auth;
use Illuminate\Support\Str;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class RestaurantJobs extends Controller{

public function all(Request $request){
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$restaurants=Auth::guard('employee')->user()->getRestaurants()->get();
$arr= array();
foreach($restaurants as $rest){array_push($arr,$rest->id);
}
$query = RestaurantJob::whereIn('restaurant_id',$arr)->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);
}

public function create(Request $request){
$input=["restaurant"=>$request->restaurant,
"title"=>$request->title,
"description"=>$request->description,
"experience"=>$request->experience,
"timing"=>$request->timing,
"skill"=>$request->skill,
"budget"=>$request->budget];
$rule=["restaurant"=>"required|max:255",
"title"=>"required|max:255",
"description"=>"required|max:255",
"experience"=>"required",
"timing"=>"required",
"skill"=>"required",
"budget"=>"required"];
$message=["restaurant.required"=>"Please select restaurant",
"title.required"=>"Title is mandetory",
"description.required"=>"Description is mandetory",
"experinece.required"=>"Experinece is required",
"timing.required"=>"Timing is mandetory",
"skill.required"=>"Skills are mandetory",
"budget.required"=>"Budget is required"];
$validator=Validator::make($input, $rule,$message);
if($validator->fails()){return response()->json($validator->errors());
}
$job= new RestaurantJob();
$job->restaurant_id=$request->restaurant;
$job->title=$request->title;
$job->description=$request->description;
$job->experience=$request->experience;
$job->timing=$request->timing;
$job->slug=Str::slug($request->restaurant.'_'.$request->title);
$job->skill=$request->skill;
$job->budget=$request->budget;
$count=$job->save();
if($count>0){
return response(["status"=>"Success","msg"=>"Job  created successfully."],200);
}
else{
return response(["status"=>"Faliure","msg"=>"Employee record not created successfully."],200);    
}
}

public function edit(Request $request, $slug){
$job=RestaurantJob::where('slug',$slug)->get();
if($job){return response(["status"=>"success","data"=>$job,"msg"=>"success"]);
}
}
public function update(Request $request){
$input=["restaurant"=>$request->restaurant,
"title"=>$request->title,
"description"=>$request->description,
"experience"=>$request->experience,
"timing"=>$request->timing,
"skill"=>$request->skill,
"budget"=>$request->budget];
$rule=["restaurant"=>"required|max:255",
"title"=>"required|max:255",
"description"=>"required|max:255",
"experience"=>"required",
"timing"=>"required",
"skill"=>"required",
"budget"=>"required"];
$message=["restaurant.required"=>"Please select restaurant",
"title.required"=>"Title is mandetory",
"description.required"=>"Description is mandetory",
"experinece.required"=>"Experinece is required",
"timing.required"=>"Timing is mandetory",
"skill.required"=>"Skills are mandetory",
"budget.required"=>"Budget is required"];
$validator=Validator::make($input, $rule,$message);
if($validator->fails()){
return response()->json($validator->errors());
}
$count=RestaurantJob::where('id',$request->id)->update([
"restaurant_id"=>$request->restaurant,
"title"=>$request->title,
"description"=>$request->description,
"experience"=>$request->experience,
"timing"=>$request->timing,
"skill"=>$request->skill,
"budget"=>$request->budget,
"slug"=>Str::slug($request->restaurant.'_'.$request->title)
]);
if($count>0){
return  response(["status"=>"Success","msg"=>"Record updated successfully"],200);
}          
}

public function delete(Request $request){
$count=RestaurantJob::find($request->slug)->delete();
if($count>0){
return response(["status"=>"success","msg"=>"Record deleted successfully"],200);
}
else{return response(["status"=>"faliure","msg"=>"Record does not get deleted"]);          
}
}

}
