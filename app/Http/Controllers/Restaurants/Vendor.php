<?php

namespace App\Http\Controllers\Restaurants;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Vendors;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use Auth;
class Vendor extends Controller
{

public function all(Request $request){
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$restaurants=Auth::guard('employee')->user()->getRestaurants()->get();
$arr= array();
foreach($restaurants as $rest){
array_push($arr,$rest->id);
}
$query = Vendors::whereIn('restaurant_id',$arr)->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);
}
public function create(Request $request){
$input=[
"company_name"=>$request->company,
"display"=>$request->display,
"address"=>$request->address,
"phone"=>$request->phone,
"primary_contact"=>$request->contact,                
"website"=>$request->website,];
$rule=[
"company_name"=>['required',Rule::unique('vendors')->where('restaurant_id',Auth::user()->restaurant_id)],//"required|unique:vendors,company_name",
"display"=>"required|string|max:255",
"address"=>"required|max:255",
"phone"=>"required|numeric",
"primary_contact"=>['required',Rule::unique('vendors')->where('restaurant_id',Auth::user()->restaurant_id)],
"website"=>['required',Rule::unique('vendors')->where('restaurant_id',Auth::user()->restaurant_id)]//"required|unique:vendors,website",
];
$message=[
"company_name.required"=>"Company  name is required",
"display.required"=>"Display name is required",
"address.required"=>"address is required",
"phone.required"=>"Phone number is required",
"phone.numeric"=>"Phone number must be numeric",
"primary_contact"=>"Primary contact number is required",
"website.required"=>"website is required",];
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()){return response(['status'=>'validErr','data'=>$validator->errors()]);
}
$vendor= new Vendors();
$vendor->restaurant_id=Auth::user()->restaurant_id;
$vendor->company_name=$request->company;
$vendor->display_name=$request->display;
$vendor->slug=Str::slug($request->company);
$vendor->address=$request->address;
$vendor->phone=$request->phone;               
$vendor->primary_contact=$request->contact;
$vendor->website=$request->website;  
$count=$vendor->save();
if($count>0){return response(["status"=>"success","msg"=>"Vendor Created Successfully"],200);
}
else{return response(["status"=>"faliure","msg"=>"Something went wrong"],200);               	
}
}
public function edit(Request $request, $slug)
{

$vendor=Vendors::where('slug',$slug)->get();
return response(["status"=>"success","data"=>$vendor,"msg"=>"Record get Successfully"],200);
}
public function update(Request $request){
$input=[
"company_name"=>$request->company,
"display"=>$request->display,
"address"=>$request->address,
"phone"=>$request->phone,
"primary_contact"=>$request->contact,                
"website"=>$request->website,];
$rule=[
"company_name"=>['required',Rule::unique('vendors')->where('restaurant_id',Auth::user()->restaurant_id)
->where('id','<>',$request->id)],//"required|unique:vendors,company_name,".$request->id,
"display"=>"required|string|max:255",
"address"=>"required|max:255",
"phone"=>"required|numeric",
"primary_contact"=>['required',Rule::unique('vendors')->where('restaurant_id',Auth::user()->restaurant_id)
->where('id','<>',$request->id)],//"required|numeric|unique:vendors,primary_contact,".$request->id,
"website"=>['required','url',Rule::unique('vendors')->where('restaurant_id',Auth::user()->restaurant_id)
->where('id','<>',$request->id)]//"required|url",
];
$message=[
"company_name.required"=>"company is required",
"display.required"=>"display is required",
"address.required"=>"address is required",
"Phone.required"=>"Phone is required",
"primary_contact.required"=>"contact is required",
"website.required"=>"website is required",];
$validator=Validator::make($input,$rule,$message);
if ($validator->fails()) {return response(['status'=>'validErr','data'=>$validator->errors()]);
}
$count=Vendors::where('id',$request->id)->update([
"restaurant_id"=>Auth::user()->restaurant_id,
"company_name"=>$request->company,
"display_name"=>$request->display,
"slug"=>Str::slug($request->company),
"address"=>$request->address,
"phone"=>$request->phone,
"primary_contact"=>$request->contact,
"website"=>$request->website         
]);
if($count>0){return response(["status"=>"success","msg"=>"Record Updated Successfully"],200);
}
else{
return response(["status"=>"faliure","msg"=>"Something went wrong"],200);         	
}
}
public function delete(Request $request){
$count=Vendors::where('id',$request->id)->delete();
if($count>0){
return response(["status"=>"success","msg"=>"Record deleted successfully"],200);
}
else{return response(["status"=>"faliue","msg"=>"Something went wrong"],200);       	
}
}
}
