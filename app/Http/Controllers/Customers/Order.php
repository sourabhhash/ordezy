<?php
namespace App\Http\Controllers\Customers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Restaurant;
use DB;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\OrderItem;
use App\Models\RestaurantWorkspace;
use Stripe\Stripe;
use Auth;
use App\Models\Customer;
use GuzzleHttp\Client;
use App\Models\RestaurantMenu;
use App\Models\RestaurantSubcategory;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Encryption\Encrypter;
use App\Models\Order as MyOrder;
class Order extends Controller{
/********************************************************************** */
public function getRestaurant(){   
$data=DB::table('restaurants')->join('restaurant_workspaces','restaurants.id',
'restaurant_workspaces.restaurant_id')->select('restaurant_workspaces.profile_img','restaurants.id', 'restaurants.name')->get();
return response(["status"=>"success","data"=>$data],200);
}
/************************************************************************************ */
public function getCart(Request $request){
$rest=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->first();
$data=Cart::join('cart_items','cart_items.cart_id','carts.id')
->join('restaurant_menus','restaurant_menus.id','cart_items.menu_id')
->join('restaurants','restaurants.id','carts.restaurant_id')
->where('carts.restaurant_id',$rest->restaurant_id)->where('carts.customer_id',Auth::user()->id)
->select('restaurant_menus.title as title','cart_items.quantity as count','cart_items.price',
'carts.total_price as total', 'cart_items.menu_id as menuid','restaurants.name as restaurant',
'restaurants.location','restaurant_menus.stock_quantity')->get();
return response(['data'=>$data,"rest"=>$rest]);
}
/********************************************************************************************* */
public function addCart(Request $request){
$data=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->get();
if(count($data)==0){
return response(['status'=>"validErr","domain"=>"not_found",'msg'=>"Domain Name does not exist"]);}
$items=json_decode($request->items);
$menu =array();
 foreach($items as $key=> $value)
 {
    $menu[$key]=$value;
 }
$restaurant=Restaurant::where('id',$data[0]->restaurant_id)->first();
$ResMenu=RestaurantMenu::where('id',$menu['menuid'])->first();
	if($restaurant->manage_stock==1 && $ResMenu->manage_stock==1)
	{

      if($ResMenu->stock_quantity <=0)
      {

 return response(["status"=>"stock_empty","msg"=>"Item not available"]);       
      }   
	}
$cart=Cart::where('customer_id',Auth::user()->id)->get();         
if(count($cart)>0){
$cart=$cart[0];
Cart::where('id',$cart->id)->where('restaurant_id',$data[0]->restaurant_id)->update(['total_price'=>$request->total]);
$items=json_decode($request->items);
if(CartItem::where('menu_id',$items->menuid)->count()>0){
CartItem::where('menu_id',$items->menuid)->
update(['quantity'=>$items->count]);
}
else{
CartItem::create(['restaurant_id'=>$data[0]->restaurant_id,
'cart_id'=>$cart->id,
'menu_id'=>$items->menuid,
'price'=>$items->price,
'quantity'=>$items->count]);               
}
}
else{
	// if($restaurant->manage_stock==1 && $ResMenu->manage_stock==1)
	// {
 //      if($ResMenu->stock_quantity <=0)
 //      {
 // return response(["status"=>"stock_empty","msg"=>"Item not available"]);       
 //      }   
	// }
$count=Cart::create([
'restaurant_id'=>$data[0]->restaurant_id,
'customer_id'=>Auth::user()->id,
'total_price'=>$request->total,
'discount'=>0,
'taxes'=>0]);          
$items=json_decode($request->items);
$cart=Cart::where('customer_id',Auth::user()->id)->where('restaurant_id',$data[0]->restaurant_id)->get();
$cart=$cart[0];
CartItem::create(['restaurant_id'=>$data[0]->restaurant_id,
'cart_id'=>$cart->id,
'menu_id'=>$items->menuid,
'price'=>$items->price,
'quantity'=>$items->count]);   
}
//RestaurantMenu::where('id',$menu['menuid'])->update(['stock_quantity'=>DB::raw('stock_quantity-1')]);
return response(['status'=>'success']);
   }
/******************************************************************************************* */
public function updateCart(Request $request){


$items=json_decode($request->items,true);
 $menu =array();
 foreach($items as $key=> $value)
 {
    $menu[$key]=$value;
 }
$data=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->get(); 
 $restaurant=Restaurant::where('id',$data[0]->restaurant_id)->first();
$ResMenu=RestaurantMenu::where('id',$menu['menuid'])->first();
	if($restaurant->manage_stock==1 && $ResMenu->manage_stock==1)
	{
      if($ResMenu->stock_quantity <=0)
      {
 return response(["status"=>"stock_empty","msg"=>"Item not available"]);       
      }   
	}
//  $stock=RestaurantMenu::where('id',$menu['menuid'])->select('stock_quantity')->first()->stock_quantity;

// if(isset($stock))
// {
// if($stock<=0){
// return response(["status"=>"stock_empty","msg"=>"Item not available"]);
// }
// } 
  
Cart::where('customer_id',Auth::user()->id)->update(['total_price'=>$request->total]);

if($menu['count']<=0){Cart::where('customer_id',Auth::user()->id)->update(['total_price'=>$request->total]);
CartItem::where('menu_id',$menu['menuid'])->delete();

}
else{

$cart=Cart::where('customer_id',Auth::user()->id)->first()->id;
cartItem::where('cart_id',$cart)->where('menu_id',$menu['menuid'])->update(["price"=>$menu['price'],
"quantity"=>$menu['count']]);        
}
//RestaurantMenu::where('id',$menu['menuid'])->update(['stock_quantity'=>DB::raw('stock_quantity+1') ]);
return response(["status"=>"success","msg"=>"cart updated sucessfully"]); 
} 
/**************************************************************************** */
public function deleteCart(Request $request){
$cart=Cart::where('customer_id',Auth::user()->id)->first()->id;
CartItem::where('cart_id',$cart)->delete();
Cart::where('id',$cart)->delete();
return response(['status'=>"success","msg"=>'cart deleted successfully']);
} 
/************************************************************************************ */
public function placeOrder(Request $request){ 
$cart=Cart::where('restaurant_id',Auth::user()->restaurant_id)->where('customer_id',Auth::user()->id)->first();
if(!empty($cart)){
$citem=CartItem::where('cart_id',$cart->id)->with('menu')->get();
$data=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->get(); 
$restaurant=Restaurant::where('id',$data[0]->restaurant_id)->first();
//$ResMenu=RestaurantMenu::where('id',$menu['menuid'])->first();
foreach($citem as $item){
// if($item->menu['stock_quantity']<1){
// return response(['status'=>"stock_empty",'msg'=>'Item out of stock']);
// }
	$ResMenu=RestaurantMenu::where('id',$item->menu['id'])->first();

	if($restaurant->manage_stock==1 && $item->menu['manage_stock']==1)
	{

      if($item->quantity > $ResMenu->stock_quantity)
      {
        return response(["status"=>"stock_empty","msg"=>"Item not available"]);       
      }   
	}
}
$count=MyOrder::where('cart_id',$cart->id)->where('restaurant_id',Auth::user()->restaurant_id)->
where('customer_id',Auth::user()->id)->count();
if( $count>0){
return response(['status'=>"order_exists","msg"=>"Order already Placed"]);
}
 }
 //'sk_test_51IRbFSKpHgZ7nxKRrPOohRzbcR1t3q46joSzoDrh0u3crDyqAMlcLe45v9kOCWsGEmyTuoEOiqJXc8eAzoxeVNJG00zhTaGvrZ'
 // Stripe::setApiKey(Auth::user()->stripe_secret_key);
 Stripe::setApiKey('sk_test_EVpAetFXaIx3IOmN0YlBq0ag');
 $stripe = new \Stripe\StripeClient(Auth::user()->stripe_secret_key);
if(Auth::user()->stripe_id )
{
$customer= $stripe->customers->retrieve(Auth::user()->stripe_id);
if($customer->email!=Auth::user()->email)
{
$stripe->customers->updateSource(
Auth::user()->stripe_id,
Auth::user()->stripe_card_id,
['email' => Auth::user()->email]
);
  }
}
else
{
$customer = $stripe->customers->create([
'name' => Auth::user()->first_name,
'address' => [
'line1' => Auth::user()->address ? Auth::user()->address :'address' ,
'postal_code' => Auth::user()->zip ? Auth::user()->zip : '000000',
'city' => Auth::user()->city ? Auth::user()->city : 'city', 
'state' => 'CA',
'country' => 'US',
],
'email'=>Auth::user()->email              
  ]);
}
try{
$stripe_token= $stripe->tokens->create([
'card' => [
'number' => Crypt::decryptString($request->card_number),
'exp_month' => Crypt::decryptString($request->expiry_month),
'exp_year' => Crypt::decryptString($request->expiry_year),
'cvc' => Crypt::decryptString($request->card_cvv)],]); 
$card = $stripe->customers->createSource($customer->id,['source' => $stripe_token->id]);
Customer::where('id',Auth::user()->id)->update(['stripe_id'=>$customer->id, 'stripe_card_id'=>$card->id]);    
 }
catch (Stripe\Exception\CardException $ex) {
$msg=$ex->getMessage();
return response(["status"=>"faliure","msg"=>$msg]);
}
try{
$data= $stripe->charges->create([
'amount' =>$cart->total_price*100,
'currency' => 'usd',
'description' => 'Amount Paid ',
'customer'=>$customer
]);
 }
catch(Stripe\Exception\InvalidRequestException $ex){
 $msg=$ex->getMessage();
 return response(["status"=>"faliure","msg"=>$msg]);
}
$quantity=CartItem::where('cart_id',$cart->id)->select(DB::raw('sum(quantity) as quantity'))->get();
$order= new MyOrder();
$order->restaurant_id=$cart->restaurant_id;
$order->customer_id=$cart->customer_id;
$order->stripe_customer_id=$data->customer;
$order->cart_id=$cart->id;
$order->employee_id=100;
$order->quantity=$quantity[0]->quantity;
$order->total_amount=$cart->total_price;
$order->shipping_cost=0;
$order->coupon_code='no';
$order->payment_type="CARD";
$order->trsansaction_id=$data->balance_transaction;
$order->total_calories=0;
$order->status='pending';
$count=$order->save();
//$menu=CartItem::where('cart_id',$cart->id)->select('quantity','menu_id','price')->get();
$citem=CartItem::where('cart_id',$cart->id)->with('menu')->get();
foreach($citem as $item){
OrderItem::create([
'menu_id'=>$item->menu_id,
'order_id'=>$order->id,
'title'=>$item->menu['title'],
'price'=>$item->price,
'quantity'=>$item->quantity,
'status'=>'Y'   
]); 
RestaurantMenu::where('id',$item->menuid)->update(['stock_quantity'=>DB::raw('stock_quantity-1') ]);
}
Cart::where('id',$cart->id)->delete();
CartItem::where('cart_id',$cart->id)->delete();

if($count>0){
  return response(["status"=>"success","msg"=>"Order Placed Successfully"]);
}
else{
  return response(["status"=>"faliure","msg"=>"something went wrong"]);
}  
}
}
