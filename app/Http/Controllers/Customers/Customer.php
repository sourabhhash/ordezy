<?php

namespace App\Http\Controllers\Customers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer as MyCustomer;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\URL;
use App\Models\RestaurantWorkspace;
use Illuminate\Support\Facades\Crypt;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
class Customer extends Controller
{
public function logout()
{
	Auth::logout();
	return response(["status"=>"success"]);
}	
public function login(Request $request){
$validator = Validator::make($request->all(),[
'email' => 'required|string|email|max:255',
'password'=> 'required']);
if ($validator->fails()){
return response()->json($validator->errors());
}
$credentials = $request->only('email', 'password');
$token=null;
if($token=Auth::guard('customer')->attempt($credentials)){
return response(["status"=>"success","token"=>$token,"user"=>'customer',],200);
} 
return response(["status"=>"auth_fail","msg"=>"Invalid Credentials"]);       
}
public function all(Request $request){
$restaurant=Auth::guard('employee')->user()->restaurant_id;
$length = $request->input('length');
$sortBy = $request->input('column');
$orderBy = $request->input('dir');
$searchValue = $request->input('search');
$query = MyCustomer::where('restaurant_id',$restaurant)->eloquentQuery($sortBy, $orderBy, $searchValue);
$data = $query->paginate($length);
return new DataTableCollectionResource($data);
}
public function create(Request $request){
$input=[
"first_name"=>$request->first_name,
"email"=>$request->email,
"password"=>$request->password,
"password_confirmation"=>$request->password_confirmation,
"contact"=>$request->contact,
"gender"=>$request->gender,
// "country"=>$request->country,
//  "city"=>$request->city,
//  "state"=>$request->state,
//  "street"=>$request->street,
//  "zip"=>$request->zip,
];
$rules=[
"first_name"=>"required",
"email"=>"required|email|unique:customers,email",
"password"=>"required|confirmed",
"contact"=>"required",
"gender"=>"required",
//  "country"=>"required",
//  "city"=>"required",
//  "state"=>"required",
//  "street"=>"required",
//  "zip"=>'required'  
];
$messages=[
"first_name.required"=>"Name is mandetory.",
"email.required"=>"Email is mandetory.",
"email.email"=>"Email format is not valid.",
"contact.required"=>"Contact number is mandetory.",
"password.required"=>"Password is mandetory",
"gender.required"=>"Gender is mandetory",
//  "country.required"=>"Country is mendatory.",
//  "city.required"=>"City is mandetory.",
//  "state.required"=>"State is mandetory",
//  "street.required"=>"Street is Mandetory",
//  "zip.required"=>"Zip is Mandetory",
];    
$validator=Validator::make($input,$rules,$messages);                   
if($validator->fails()){
return response(['status'=>"validErr",'data'=>$validator->errors()]);
}
$data=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->get();
if(count($data)==0){
return response(['status'=>"validErr","domain"=>"not_found",'msg'=>"Domain Name does not exist"]);
}
$customer= new MyCustomer();
$customer->first_name=$request->first_name;
$customer->restaurant_id=$data[0]->restaurant_id;
$customer->last_name=$request->last_name;
$customer->password=Hash::make($request->password);
$customer->uuid=(string)Str::uuid();
$customer->email=$request->email;
$customer->contact_number=$request->contact;
$customer->gender=$request->gender;
$customer->city=$request->city;
$customer->state=$request->state;
$customer->country=$request->country;
$customer->street=$request->street;
$customer->zip=$request->zip;
$count=$customer->save();
if($count>0){return response(["status"=>"success","msg"=>"Record created successfully"]);
}
else{return response(["status"=>"faliure","msg"=>"Something went wrong"]);         	
}
}
public function edit(Request $request,$slug){
$data=MyCustomer::where('uuid',$slug)->get();
if($data->count()>0){
return response(["status"=>"success","data"=>$data,"msg"=>"success"]);
}
else{return response(["status"=>"faliure","msg"=>"No Data Found"]);       	
}
}
public function update(Request $request){
$input=[
"first_name"=>$request->first_name,
"email"=>$request->email,
"password"=>$request->password,
"contact"=>$request->contact,
"gender"=>$request->gender,
"password"=>$request->password,
"country"=>$request->country,
"city"=>$request->city,
"state"=>$request->state,
"street"=>$request->street,
"zip"=>$request->zip,];
$rules=[
"first_name"=>"required",
"email"=>"required|email",
"password"=>"required",
"contact"=>"required",
"password"=>"required",
"gender"=>"required",
"country"=>"required",
"city"=>"required",
"state"=>"required",
"street"=>"required",
"zip"=>'required'];
$messages=[
"first_name.required"=>"Name is mandetory.",
"email.required"=>"Email is mandetory.",
"email.email"=>"Email formait is not valid.",
"contact.required"=>"Contact number is mandetory.",
"password.required"=>"Password is mandetory",
"gender.required"=>"Gender is mandetory",
"country.required"=>"Country is mendatory.",
"city.required"=>"City is mandetory.",
"state.required"=>"State is mandetory",
"street.required"=>"Street is Mandetory",
"zip.required"=>"Zip is Mandetory",];    
$validator=Validator::make($input,$rules,$messages);                   
if($validator->fails()){
return response()->json($validator->errors());
}
$count=Mycustomer::where('uuid',$request->slug)->update([
"first_name"=>$request->first_name,
"last_name"=>$request->last_name,
"email"=>$request->email,
"password"=>$request->password,
"contact_number"=>$request->contact,
"gender"=>$request->gender,
"password"=>$request->password,
"country"=>$request->country,
"city"=>$request->city,
"state"=>$request->state,
"street"=>$request->street,
"zip"=>$request->zip,]);
if($count>0){
  return response(["status"=>"Success","msg"=>"Record Updated Successfully"]);
}
else{
return response(["status"=>"faliure","msg"=>"Something went wrong"]);       	
}
}
public function delete(Request $request){
$count=MyCustomer::where('uuid',$request->uuid)->delete();
if($count>0){
return response(["status"=>"Success","msg"=>"Record deleted successfully"]);
}
else{
return response(["status"=>"Faliure","msg"=>"Something went wrong."]);
}
}
}
