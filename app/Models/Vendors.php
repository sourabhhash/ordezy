<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class Vendors extends Model
{
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'company_name' => ['searchable' => true,],
'address' => ['searchable' => true,],
'website' => ['searchable' => true,],
'slug' => ['searchable' => false,],
'id'=>[]
];
protected $dataTableRelationships = [];
}
