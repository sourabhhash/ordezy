<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class MenuItemAttribute extends Model
{
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'title' => ['searchable' => true,],
'price' => ['searchable' => true,],
'description' => [],
'slug'=>[],
'id'=>[]   
];
protected $dataTableRelationships = [
"belongsTo" => [
"menu" => [
"model" => \App\Models\RestaurantMenu::class,
"foreign_key" => "menu_id",
"columns" => [
"title" => ["searchable" => true,]
             ],
               ],
             ]
];
public function menu(){
return $this->belongsTo('App\Models\RestaurantMenu','menu_id');
}
}
