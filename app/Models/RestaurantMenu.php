<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use RestaurantWorkspace;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class RestaurantMenu extends Model
{
//
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'title' => ['searchable' => true,],
'price' => ['searchable' => true,],
'calories' => [],
'slug' => [],
'id' => [],
'thumbnail_pic'=>[],
];
protected $dataTableRelationships = 
[
"belongsTo" => [
"restaurant" => [
"model" => \App\Models\Restaurant::class,
"foreign_key" => "restaurant_id",
"columns" => [
"name" => ["searchable" => true,]
             ],
               ],
             ]
];
public function restaurant(){
return  $this->belongsTo('App\Models\Restaurant');
}
public function menuItem(){
return $this->hasMany('App\Models\MenuItemAttribute','menu_id');
}
public function category(){
//      $restaurant=RestaurantWorkspace::where('domain', 'like','%'.request()->getHost().'%')->first();
return $this->belongsTo('App\Models\RestaurantSubcategory','category_id','id');
} 
}
