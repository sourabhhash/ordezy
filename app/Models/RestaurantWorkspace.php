<?php

namespace App\Models;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
use Illuminate\Database\Eloquent\Model;

class RestaurantWorkspace extends Model
{
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'domain' => ['searchable' => true,],
'brand_color' => ['searchable' => true,]
];
protected $dataTableRelationships = [
"belongsTo" => [
"Restaurants" => [
"model" => \App\Models\Restaurant::class,
"foreign_key" => "restaurant_id",
"columns" => [
"name" => ["searchable" => true,]
             ],
                 ],
              ]
];
public function Restaurants(){
return 	$this->belongsTo(Restaurant::class,'restaurant_id','id');
}
}
