<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Restaurant;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class RestaurantJob extends Model
{
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'title' => ['searchable' => true,],
'description' => ['searchable' => true,],
'experience' => ['searchable' => true,],
'slug' => ['searchable' => false,]
];
protected $dataTableRelationships = [];    
public function restaurant(){
return $this->belongsTo(Restaurant::class);
}
}
