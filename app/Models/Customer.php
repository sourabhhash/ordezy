<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Subscription;
use Illuminate\Support\Facades\Crypt;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class Customer extends Authenticatable   implements JWTSubject
{
use Billable;
use Notifiable;
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'first_name' => ['searchable' => true,],
'email' => ['searchable' => true,],
'contact_number' => [ ],
'uuid' => [],
];
protected $guard = 'customer';
public function getJWTIdentifier(){
return $this->getKey();
}
public function getJWTCustomClaims(){
return ['user'=>encrypt('customer')
];
}
protected $dataTableRelationships = [];       
}
