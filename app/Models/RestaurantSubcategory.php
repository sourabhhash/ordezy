<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RestaurantCategory;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
use App\Models\Restaurant;
class RestaurantSubcategory extends Model
{
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'parent_id'=>[],
'id'=>[],
'parent_title'=>["searchable"=>true,],
'slug' => [ ],
'title' => ['searchable' => true,],

];
protected $dataTableRelationships = [] ;
public function RestaurantCategory(){
return $this->belongsTo(RestaurantCategory::class,'category_id','id');
}
public function Restaurants(){
return 	$this->belongsTo(Restaurant::class,'restaurant_id','id');
}
}
