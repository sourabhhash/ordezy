<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class Coupon extends Model
{
//
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'title' => 
['searchable' => true,],
'code' => ['searchable' => true,],
'type' => [],
'slug'=>[]   
];
protected $dataTableRelationships = [];  
}
