<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
use App\Models\OrderItem;
use App\Models\Customer;
use Carbon\Carbon;
class Order extends Model
{
//
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'customer_id' => ['searchable' => true,],
'quantity' => ['searchable' => true,],
'total_amount' => [],
];
public function itemsOfOrder(){
    return $this->hasMany(OrderItem::class,'order_id');
  }
public function customer(){
  return $this->belongsTo(Customer::class);
}
public function getCreatedAtAttribute($date)
{
  return Carbon::parse($date)->format('d-M-Y H:i:s');
}
}
