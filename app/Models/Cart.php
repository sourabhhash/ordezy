<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
   protected $fillable=['restaurant_id','customer_id','total_price','discount','taxes'];
}
