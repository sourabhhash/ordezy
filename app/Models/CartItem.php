<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
protected $fillable=['restaurant_id','cart_id','menu_id','price','quantity'];
public function menu(){
return $this->belongsTo('App\Models\RestaurantMenu');
}
}
