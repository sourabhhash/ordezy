<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Order;
use App\Models\RestaurantMenu;
class OrderItem extends Model
{
    protected $fillable=['menu_id','order_id','title','price','quantity','status'];
  public function items(){
      return $this->hasMany(RestaurantMenu::class,'id','menu_id');
  }

}
