<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RestaurantCategory;
use App\Models\RestaurantMenu;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class Restaurant extends Model
{
// 
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'name' => ['searchable' => true,],
'email' => ['searchable' => true,],
'website' => ['searchable' => true,]
];
protected $dataTableRelationships = [
];
public function RestaurantCategory(){
return $this->hasMany(RestaurantCategory::class,'restaurant_id','id');
}
public function RestaurantMenu(){   
return $this->hasMany(RestaurantMenu::class);
}



}
