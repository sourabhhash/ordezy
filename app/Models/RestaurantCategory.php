<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RestaurantSubcategory;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
use App\Models\Restaurant;
class RestaurantCategory extends Model
{
    //
        use LaravelVueDatatableTrait;
    protected $dataTableColumns = [

        'title' => [
            'searchable' => true,
        ],
        'slug' => [
            'searchable' => true,
        ]
    ];
    protected $dataTableRelationships = [

        "belongsTo" => [
            "Restaurants" => [
                "model" => \App\Models\Restaurant::class,
                "foreign_key" => "restaurant_id",
                "columns" => [
                    "name" => [
                        "searchable" => true,

                    ]
                ],
            ],
        ]
        
    ];
    public function RestaurantSubcategory()
    {

      return 	$this->hasMany(RestaurantSubcategory::class,"category_id","id");
    }
    public function Restaurants()
    {
      return 	$this->belongsTo(Restaurant::class,'restaurant_id','id');
    }
}
