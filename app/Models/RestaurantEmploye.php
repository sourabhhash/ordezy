<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Restaurant;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
class RestaurantEmploye extends Authenticatable   implements JWTSubject
{
use Notifiable;
protected $guard = 'employee';
use LaravelVueDatatableTrait;
protected $dataTableColumns = [
'name' => ['searchable' => true,],
'email' => ['searchable' => true,],
'contact_number' => [],
'role' => ['searchable' => true,],
'uuid'=>[]
];
protected $dataTableRelationships = [];  
public function getJWTIdentifier(){
return $this->getKey();
}

/**
* Return a key value array, containing any custom claims to be added to the JWT.
*
* @return array
*/
public function getJWTCustomClaims(){
return ['user'=>'restaurant'];
}
public function getRestaurants(){
return $this->belongsTo(Restaurant::class,'restaurant_id','id');
}
}
