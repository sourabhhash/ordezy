/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Vue.component('main-component', require('./Main.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Vuelidate from 'vuelidate';
import VueBootstrapToasts from "vue-bootstrap-toasts";
 import FlashMessage from '@smartweb/vue-flash-message';
import Base64 from 'crypto-js/enc-base64';



Vue.use(VueBootstrapToasts);
Vue.use(Vuelidate);
Vue.use(VueAxios, axios);
Vue.component('pagination', require('laravel-vue-pagination'));
import DataTable from 'laravel-vue-datatable';
Vue.use(DataTable);
 Vue.use(FlashMessage);

axios.interceptors.request.use(
  config => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers.common["Authorization"] = 'Bearer ' + token;
    }
    console.log(config,'config');
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);


Vue.config.productionTip = false;

const app = new Vue({
    el: '#app',
    component: {
    	App
    },
    router,
     render: h => h(App)
});
