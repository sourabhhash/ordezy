import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
Vue.use(VueRouter);
const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: require('./Home.vue').default,
    },
    {
      path: '/profile',
      component: require('./profile.vue').default,
    },
    {
      path: '/profilepayment',
      component: require('./profilepayment.vue').default,
    },
    {
      path: '/restaurant/login',
      name: "login",
      component: require('./views/RestaurantEmployees/EmployeeLogin.vue').default,
    },

    {
      path: '/restaurant',
      component: require('./views/RestaurantLayout.vue').default,
      children: [
        {
          path: '/',
          component: require('./views/RestaurantMenus/All.vue').default,
        },

        {
          path: 'job',
          name: "job",
          component: require('./views/RestaurantJobs/AllJobs.vue').default,
        },

        {
          path: 'job/create',
          component: require('./views/RestaurantJobs/AddJob.vue').default,
        },
        {
          path: 'job/edit/:slug',
          component: require('./views/RestaurantJobs/EditJob.vue').default,
        },
        {
          path: 'category',
          component: require('./views/RestaurantSubcategory/All.vue').default,
        },
        {
          path: 'category/create',
          component: require('./views/RestaurantSubcategory/AddSubcategory.vue').default,
        },

        {
          path: 'category/edit/:slug',
          component: require('./views/RestaurantSubcategory/EditSubcategory.vue').default,
        },
        {
          path: 'menu',
          component: require('./views/RestaurantMenus/All.vue').default,
        },
        {
          path: 'menu/create',
          component: require('./views/RestaurantMenus/AddMenu.vue').default,
        },
        {
          path: 'menu/edit/:slug',
          component: require('./views/RestaurantMenus/EditMenu.vue').default,
        },
        {
          path: 'vendor',
          component: require('./views/RestaurantVendor/All.vue').default,
        },
        {
          path: 'vendor/create',
          component: require('./views/RestaurantVendor/AddVendor.vue').default,
        },
        {
          path: 'vendor/edit/:slug',
          component: require('./views/RestaurantVendor/EditVendor.vue').default,
        },
        {
          path: 'setting',
          component: require('./views/RestaurantWorkspace/EditWorkspace.vue').default,
        },
        {
          path: 'menu-item',
          component: require('./views/MenuItems/All.vue').default,
        },
        {
          path: 'menu-item/add',
          component: require('./views/MenuItems/AddMenuItem.vue').default,
        },
        {
          path: 'menu-item/edit/:slug',
          component: require('./views/MenuItems/EditMenuItem.vue').default,
        },
        {
          path: 'client',
          component: require('./views/Customers/All.vue').default,
        },
        {
          path: 'employee',
          component: require('./views/RestaurantEmployees/All.vue').default,
        },
        {
          path: 'employee/create',
          component: require('./views/RestaurantEmployees/AddRestaurantEmployee.vue').default,
        },
        {
          path: 'employee/edit/:uuid',
          component: require('./views/RestaurantEmployees/EditRestaurantEmployee.vue').default,
        },

        {
          path: 'coupon/all',
          component: require('./views/RestaurantCoupen/All.vue').default,

        },
        {
          path: 'coupon/create',
          component: require('./views/RestaurantCoupen/AddCoupen.vue').default,

        },
        {
          path: 'coupon/edit/:slug',
          component: require('./views/RestaurantCoupen/EditCoupon.vue').default,

        },
        {
          path: 'order/all',
          component: require('./views/Orders/list.vue').default,

        },
        {
          path: 'cheff/orders',
          component: require('./views/Cheff/order.vue').default,
        },
        {
          path: 'cheff/order/process',
          component: require('./views/Cheff/OrderProcess.vue').default,
        },
        {
          path: 'order/assign',
          component: require('./views/Orders/AssignOrder.vue').default,
        }                        
      ]
    },
    {
      path: '/user/order/payment',
      component: require('./views/Orders/payment.vue').default,
    },
    {
      path: '/user/create',
      component: require('./views/Customers/AddCustomer.vue').default,
    },

    {
      path: '/user/edit/:slug',
      component: require('./views/Customers/EditCustomer.vue').default,
    },
    {
      path: '/user/login',
      component: require('./views/Customers/Login.vue').default,
    },
    {
      path: '/restaurant/reservation/create',
      component: require('./views/Reservations/AddReservation.vue').default,

    },
    {
      path: '/restaurant/reservation/all',
      component: require('./views/Reservations/All.vue').default,
    },
    {
      path: '/restaurant/reservation/edit/:id',
      component: require('./views/Reservations/EditReservation.vue').default,
    },
    {
      path: '/order/restaurants',
      component: require('./views/Orders/restaurants.vue').default,
    },
    {
      path: '/order/restaurant/:id/menu/',
      component: require('./views/Orders/RestaurantMenu.vue').default,
    },
    {
      path: '/order/list',
      component: require('./views/Orders/list.vue').default,
    },
    {
      path: '/order/placeorder/:id/:rest',
      component: require('./views/Orders/PlaceOrder.vue').default,
    },

    {
      path: '/restaurant/create',
      component: require('./views/Restaurants/AddRestaurant.vue').default,
    },
    {
      path: '/restaurant/edit/:slug',
      component: require('./views/Restaurants/EditRestaurant.vue').default,
    },

    {
      path: '/restaurant/employee/login',
      component: require('./views/RestaurantEmployees/EmployeeLogin.vue').default,
    },
    {
      path: '/customer/order/placed',
      component: require('./views/Orders/OrderPlaced.vue').default,
    },

    // {
    //   path: '/restaurant/menu-attributes/edit/:id',
    //   component: require('./views/MenuItems/EditMenuAttribute.vue').default,
    // },


  ]
});

var status;
 router.beforeEach(
  async (to, from, next) => {
    

    if (to.fullPath.startsWith('/restaurant') && to.fullPath != "/restaurant/login") 
    {
// if(localStorage.getItem('user')=='customer')
// {
//   localStorage.setItem('user','');
//   localStorage.setItem('token','');
//  next('/restaurant/login')
// }

    }
 
    if (to.fullPath.startsWith('/user') && to.fullPath != "/user/login") 
    {
      // if(localStorage.getItem('user')=='employee')
      // {
      //   localStorage.setItem('user','');
      //   localStorage.setItem('token','');
      //   next('/user/login')
      // }

     
     }  
next();
 
  });
//   function  checkToken(type) {

//   let formdata= new FormData();
//   formdata.append('user',type);
//    return axios({
//     url:"/api/checkToken",
//     method:"post",
//     data:formdata
//   }).then(response=>
//     {
//        console.log(response);

//     return response.data.status;
//     }).catch(errors=>{
//        return 'error';

//    });


// }
export default router;