<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Ordeazy</title>
   <link rel="stylesheet" href="{{asset('css/bootstrap/css/bootstrap.min.css')}}">
   <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
   <script src="https://js.stripe.com/v3/"></script>
   <script src="https://use.fontawesome.com/1337f7fe41.js"></script>
   <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
   <link rel="stylesheet" href="{{asset('fonts/fonts.css')}}">
   <link rel="stylesheet" href="{{asset('css/f_style.css')}}">
   <link rel="stylesheet" href="{{asset('css/app.css')}}">
   <style>
      :root{
         --brand-color: var(--orange);
      }   
   </style>
</head>
<body>

   <div id="app"></div>

   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{asset('css/bootstrap/js/bootstrap.min.js')}}">
   <script src="https://checkout.stripe.com/checkout.js"></script>
   <script src="https://unpkg.com/feather-icons"></script>
   <script src="{{ mix('js/app.js') }}"></script>
   <script>
      feather.replace()
   </script>
  <!--<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
      data-key="sk_test_51IRbFSKpHgZ7nxKRrPOohRzbcR1t3q46joSzoDrh0u3crDyqAMlcLe45v9kOCWsGEmyTuoEOiqJXc8eAzoxeVNJG00zhTaGvrZ"
      data-description="Access for a year"
      data-amount="5000"
      data-locale="auto">
   </script> -->

</body>
</html>
