<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Encryption\Encrypter;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('checkToken','checkToken@check');
/*--------------List Of All Restaurants for Super Admin or For Customer------------------*/
Route::get("getallRestaurants",'Restaurants\AllRestaurants@getAllRestaurants');
/*------------------------------------------------------------------------------*/
Route::get('getMenuByRestaurant/{id}','Restaurants\AllRestaurants@getMenuByRestaurant');
Route::middleware(['jwt.verify'])->group(function () {
    //
/*************************************Restaurant Routes**************************************/    
Route::get('allRestaurants','Restaurants\AllRestaurants@all');
Route::get('allRestaurant','Restaurants\AllRestaurants@all_restaurants');
Route::post('createRestaurant','Restaurants\AllRestaurants@create');
Route::get('editRestaurant/{slug}','Restaurants\AllRestaurants@edit');
Route::post('updateRestaurant','Restaurants\AllRestaurants@update');
Route::post('deleteRestaurant','Restaurants\AllRestaurants@delete');
Route::get('getMenu','Restaurants\AllRestaurants@getMenu');
Route::get("getRestaurantCategory","Restaurants\AllRestaurants@getCategory");
Route::get("getRestaurantSubcategory/{id}","Restaurants\AllRestaurants@getSubcategory");
Route::get("getSubcategoryByCategory/{id}","Restaurants\AllRestaurants@getSubcategoryByCategory");
Route::get('getRestaurant','Restaurants\AllRestaurants@getRestaurant');
/**********************************************************************************************/

/********************Restaurant jobs Routes*****************************************/
Route::post('createJob','Restaurants\RestaurantJobs@create');
Route::get('allJobs','Restaurants\RestaurantJobs@all');
Route::get('editJob/{id}','Restaurants\RestaurantJobs@edit');
Route::post('updateJob','Restaurants\RestaurantJobs@update');
Route::post('deleteJob','Restaurants\RestaurantJobs@delete');
/***************************************************************************************/

/*******************************Restaurant subcategory Routes************************/
Route::post("createRestaurantSubcategory","Restaurants\RestaurantSubcategories@create");
Route::get("allRestaurantSubcategory","Restaurants\RestaurantSubcategories@all");
Route::get("editRestaurantSubcategory/{slug}","Restaurants\RestaurantSubcategories@edit");
Route::post("updateRestaurantSubcategory","Restaurants\RestaurantSubcategories@update");
Route::post("deleteRestaurantSubcategory","Restaurants\RestaurantSubcategories@delete");
//Route::get("getRestaurantSubcategory/{id}","Restaurants\RestaurantSubcategories@getSubcategory");
/***************************************************************************************************/

/***************************************Restaurant Menu *********************************************/
Route::post("createRestaurantMenu","Restaurants\RestaurantMenus@create");
Route::get("allRestaurantMenu","Restaurants\RestaurantMenus@all");
Route::get("editRestaurantMenu/{slug}","Restaurants\RestaurantMenus@edit");
Route::post("updateRestaurantMenu","Restaurants\RestaurantMenus@update");
Route::post("deleteRestaurantMenu","Restaurants\RestaurantMenus@delete");
/*****************************************************************************************/

/****************************Restaurant Vendor********************************************/
 Route::post('createVendor','Restaurants\Vendor@create');
 Route::get('allVendor','Restaurants\Vendor@all');
 Route::get('editVendor/{id}','Restaurants\Vendor@edit');
 Route::post('updateVendor','Restaurants\Vendor@update');
 Route::post('deleteVendor','Restaurants\Vendor@delete');
/******************************************************************************************/

/***************************Restaurant Workspace*********************************************/
  Route::get('editRestaurantWorkspace','Restaurants\Workspace@edit');
  Route::post('updateRestaurantWorkspace','Restaurants\Workspace@update');
/**********************************************************************************************/  

/*******************************Menu Attribute*************************************/
Route::post('createMenuAttribute','Restaurants\MenuAttributes@create');
Route::get('allMenuAttribute','Restaurants\MenuAttributes@all');
Route::get('editMenuAttribute/{slug}','Restaurants\MenuAttributes@edit');
Route::post('updateMenuAttribute','Restaurants\MenuAttributes@update');
Route::post('deleteMenuAttribute','Restaurants\MenuAttributes@delete');
/***************************************************************************************/

/*****************************Couupen Routes******************************** */
Route::post('createCoupon','Restaurants\Coupon@create');
Route::get('allCoupon','Restaurants\Coupon@all');
Route::get('editCoupon/{slug}','Restaurants\Coupon@edit');
Route::post('updateCoupon','Restaurants\Coupon@update');
Route::post('deleteCoupon','Restaurants\Coupon@delete');
/*****************************Online Reservation Section*******************************/

/************************************************************************************** */

/*****************Restaurant Order section********************************/
Route::get('allOrder','Restaurants\OrderController@all');


/****************************Table Reservation*****************************************/


Route::post("createReservation","Restaurants\Reservation@create");
Route::get("allReservation","Restaurants\Reservation@all");
Route::get("editReservation/{id}","Restaurants\Reservation@edit");
Route::post("updateReservation","Restaurants\Reservation@update");
Route::post("deleteReservation","Restaurants\Reservation@delete");
/*********************************************************************************/
/***********************Customer Routes**********************************/
Route::get('allCustomer','Customers\Customer@all')->name('allCustomer');
Route::get('orderstoAssign','Restaurants\OrderController@OrderItmes');
/*************************************************************************/

/********************Restaurant Employee*************************** */
Route::post('createRestaurantEmployee','Restaurants\RestaurantEmployees@create');
Route::get('allRestaurantEmployee','Restaurants\RestaurantEmployees@all');
Route::post('deleteRestaurantEmployee','Restaurants\RestaurantEmployees@delete');
Route::get('getRestaurantEmployee/{uuid}','Restaurants\RestaurantEmployees@edit');
Route::post('updateRestaurantEmployee','Restaurants\RestaurantEmployees@update');


});

/************************Customers Route*************************************************/
Route::post('createCustomer','Customers\Customer@create')->name('createCustomer');       
Route::get('editCustomer/{slug}','Customers\Customer@edit')->name('editCustomer/{slug}');
Route::post('updateCustomer','Customers\Customer@update')->name('updateCustomer');
Route::post('deleteCustomer','Customers\Customer@delete')->name('deleteCustomer');
Route::post('loginCustomer','Customers\Customer@login');

/********************Restaurants Employess  Routes**********************/

Route::post('loginEmployee','Restaurants\RestaurantEmployees@login');

       

/*------------------------Order Routes------------------------------------------------------------*/

Route::get('menusOfRestaurant',"Restaurants\RestaurantMenus@menuOfRestaurant"); 
Route::get('customer/logout',"Customers\Customer@logout");
Route::middleware(['customer.varify'])->group(function () 
{
// Route::get('getRestaurantImg','Customers\Order@getRestaurant');
 Route::post('placeOrder','Customers\Order@placeOrder');
 Route::post('addtoCart','Customers\Order@addCart');
 Route::post('deleteCart','Customers\Order@deleteCart');
 Route::post('updateCart','Customers\Order@updateCart');
 Route::get('getCart','Customers\Order@getCart');
 
  
});
 Route::get("categoryOfRestaurant","Restaurants\RestaurantSubcategories@categoryOfRestaurant");
 Route::get("reconmendedMenuOfRestaurant","Restaurants\RestaurantMenus@reconmendedMenuOfRestaurant");
/******************************************************************************************/
Route::get("test", function()
{
  $decrypted = Crypt::decryptString('eyJpdiI6ImZZYk1SUnF2T0hxT0t3ajg4MjRlZFE9PSIsInZhbHVlIjoibEg5SlNGbEVhMmhHeldUQ3ZSc3VIYTZMeEZTVDBYYlBib1ByQjhEZUFqbz0iLCJtYWMiOiIwZDQ0NWU3MWYwMzhiNjA4YjQxMjI3ODFkYWM5NTM5NGEzMmJjZjc2ODY0MzAwNTA5ZWZkMzIxYWJiMjdjNDcwIn0=');
  dd($decrypted);
});