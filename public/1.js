(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_actions_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/actions.vue */ "./resources/js/components/actions.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var headers = {
  "Authorization": 'Bearer ' + localStorage.getItem("token")
};
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      restaurants: {},
      errors: [],
      columns: [{
        label: 'Name',
        name: 'title',
        orderable: true
      }, {
        label: 'Description',
        name: 'description',
        orderable: true
      }, {
        label: 'Experience',
        name: 'experience',
        orderable: true
      }, {
        label: 'Slug',
        name: 'slug',
        orderable: true
      }, {
        label: 'Action',
        name: 'Action',
        event: "click",
        component: _components_actions_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
        handler: this.action
      }],
      header: headers
    };
  },
  mounted: function mounted() {},
  methods: {
    add: function add() {
      this.$router.push('/restaurant/jobs/create');
    },
    deleteJob: function deleteJob(id) {
      var _this = this;

      var token = localStorage.getItem('token');
      var headers = {
        "Authorization": 'Bearer ' + localStorage.getItem("token")
      };
      var formdata = new FormData();
      formdata.append('id', id);
      axios__WEBPACK_IMPORTED_MODULE_0___default()({
        method: 'post',
        url: '/api/deleteJob',
        'headers': headers,
        data: formdata
      }).then(function (response) {
        var length = _this.restaurants.length;
        var res = _this.restaurants;

        for (var i = 0; i < length; i++) {
          if (res[i].id == id) {
            _this.restaurants.splice(i, 1);

            break;
          }
        }
      })["catch"](function (e) {
        _this.errors.push(e);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=template&id=8875820c&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=template&id=8875820c& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "page_wrap" }, [
    _vm._m(0),
    _c(
      "div",
      { staticClass: "restaurent_filter" },
      [
        _c(
          "button",
          {
            staticClass: "button button_md button-color-o",
            on: { click: _vm.add }
          },
          [_vm._v("Add")]
        ),
        _vm._v(" "),
        _c("data-table", {
          attrs: {
            columns: _vm.columns,
            url: "/api/allJobs",
            headers: _vm.header
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "page-title" }, [
      _c("h2", [_vm._v("Vendor")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/RestaurantJobs/AllJobs.vue":
/*!*******************************************************!*\
  !*** ./resources/js/views/RestaurantJobs/AllJobs.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AllJobs_vue_vue_type_template_id_8875820c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AllJobs.vue?vue&type=template&id=8875820c& */ "./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=template&id=8875820c&");
/* harmony import */ var _AllJobs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AllJobs.vue?vue&type=script&lang=js& */ "./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AllJobs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AllJobs_vue_vue_type_template_id_8875820c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AllJobs_vue_vue_type_template_id_8875820c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/RestaurantJobs/AllJobs.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AllJobs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AllJobs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AllJobs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=template&id=8875820c&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=template&id=8875820c& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AllJobs_vue_vue_type_template_id_8875820c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./AllJobs.vue?vue&type=template&id=8875820c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/RestaurantJobs/AllJobs.vue?vue&type=template&id=8875820c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AllJobs_vue_vue_type_template_id_8875820c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AllJobs_vue_vue_type_template_id_8875820c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vcmVzb3VyY2VzL2pzL3ZpZXdzL1Jlc3RhdXJhbnRKb2JzL0FsbEpvYnMudnVlIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92aWV3cy9SZXN0YXVyYW50Sm9icy9BbGxKb2JzLnZ1ZT8wYjYyIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy92aWV3cy9SZXN0YXVyYW50Sm9icy9BbGxKb2JzLnZ1ZSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdmlld3MvUmVzdGF1cmFudEpvYnMvQWxsSm9icy52dWU/ZDBkNSIsIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvdmlld3MvUmVzdGF1cmFudEpvYnMvQWxsSm9icy52dWU/NTY2NiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0EsTUFEQSxrQkFDQTtBQUNBO0FBQ0EscUJBREE7QUFFQSxnQkFGQTtBQUdBLGdCQUNBO0FBQ0EscUJBREE7QUFFQSxxQkFGQTtBQUdBO0FBSEEsT0FEQSxFQU9BO0FBQ0EsNEJBREE7QUFFQSwyQkFGQTtBQUdBO0FBSEEsT0FQQSxFQVlBO0FBQ0EsMkJBREE7QUFFQSwwQkFGQTtBQUdBO0FBSEEsT0FaQSxFQWlCQTtBQUNBLHFCQURBO0FBRUEsb0JBRkE7QUFHQTtBQUhBLE9BakJBLEVBc0JBO0FBQ0EsdUJBREE7QUFFQSxzQkFGQTtBQUdBLHNCQUhBO0FBSUEsa0ZBSkE7QUFLQTtBQUxBLE9BdEJBLENBSEE7QUFpQ0E7QUFqQ0E7QUFtQ0EsR0FyQ0E7QUFzQ0EsU0F0Q0EscUJBc0NBLEVBdENBO0FBdUNBO0FBRUEsT0FGQSxpQkFHQTtBQUNBO0FBQ0EsS0FMQTtBQU1BLGFBTkEscUJBTUEsRUFOQSxFQU9BO0FBQUE7O0FBRUE7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBLDZCQURBO0FBRUEsMEJBRkE7QUFHQTtBQUhBLFNBS0EsSUFMQSxDQUtBLG9CQUNBO0FBQ0E7QUFDQTs7QUFDQSx5Q0FDQTtBQUNBLCtCQUNBO0FBRUE7O0FBQ0E7QUFDQTtBQUVBO0FBR0EsT0FyQkEsV0FzQkE7QUFDQTtBQUNBLE9BeEJBO0FBeUJBO0FBekNBO0FBdkNBLEc7Ozs7Ozs7Ozs7OztBQ3RCQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQiwyQkFBMkI7QUFDL0M7QUFDQTtBQUNBO0FBQ0EsT0FBTyxtQ0FBbUM7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLDRCQUE0QjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQ3pDQTtBQUFBO0FBQUE7QUFBQTtBQUFzRjtBQUMzQjtBQUNMOzs7QUFHdEQ7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsNkVBQU07QUFDUixFQUFFLGtGQUFNO0FBQ1IsRUFBRSwyRkFBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBNkwsQ0FBZ0IsbVBBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBak47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBIiwiZmlsZSI6IjEuanMiLCJzb3VyY2VzQ29udGVudCI6WyI8dGVtcGxhdGU+XG4gICAgPGRpdiBjbGFzcz1cInBhZ2Vfd3JhcFwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwicGFnZS10aXRsZVwiPlxuICAgICAgICAgICAgPGgyPlZlbmRvcjwvaDI+XG4gICAgICAgIDwvZGl2PjxkaXYgY2xhc3M9XCJyZXN0YXVyZW50X2ZpbHRlclwiPlxuICAgICAgICA8YnV0dG9uIGNsYXNzPVwiYnV0dG9uIGJ1dHRvbl9tZCBidXR0b24tY29sb3Itb1wiIEBjbGljaz1cImFkZFwiPkFkZDwvYnV0dG9uPlxuICAgICAgICAgICAgPGRhdGEtdGFibGVcbiAgICAgICAgICAgICAgICA6Y29sdW1ucz1cImNvbHVtbnNcIlxuICAgICAgICAgICAgICAgIHVybD1cIi9hcGkvYWxsSm9ic1wiXG4gICAgICAgICAgICAgICAgOmhlYWRlcnM9XCJoZWFkZXJcIj5cblxuICAgICAgICAgICAgPC9kYXRhLXRhYmxlPlxuICAgICAgICA8L2Rpdj5cbjwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XG5pbXBvcnQgYWN0aW9ucyBmcm9tICcuLi8uLi9jb21wb25lbnRzL2FjdGlvbnMudnVlJztcbiAgICAgICAgY29uc3QgaGVhZGVycyA9IHsgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6J0JlYXJlciAnICsgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuZXhwb3J0IGRlZmF1bHQge1xuIGRhdGEoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHJlc3RhdXJhbnRzOnt9LFxuICAgICAgZXJyb3JzOiBbXSxcbiAgICAgIGNvbHVtbnM6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsOiAnTmFtZScsXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICd0aXRsZScsXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyYWJsZTogdHJ1ZSxcblxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ0Rlc2NyaXB0aW9uJyxcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2Rlc2NyaXB0aW9uJyxcbiAgICAgICAgICAgICAgICAgICAgb3JkZXJhYmxlOiB0cnVlLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ0V4cGVyaWVuY2UnLFxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnZXhwZXJpZW5jZScsXG4gICAgICAgICAgICAgICAgICAgIG9yZGVyYWJsZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw6ICdTbHVnJyxcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ3NsdWcnLFxuICAgICAgICAgICAgICAgICAgICBvcmRlcmFibGU6IHRydWUsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBY3Rpb24nLFxuICAgICAgICAgICAgICAgIG5hbWU6ICdBY3Rpb24nLFxuICAgICAgICAgICAgICAgIGV2ZW50OiBcImNsaWNrXCIsXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiBhY3Rpb25zLCBcbiAgICAgICAgICAgICAgaGFuZGxlcjogdGhpcy5hY3Rpb24sXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGhlYWRlcjpoZWFkZXJzLFxuICAgIH1cbiAgfSxcbiAgbW91bnRlZCgpIHsgIH0sXG4gbWV0aG9kczp7XG5cbiAgYWRkKClcbiAge1xuICAgIHRoaXMuJHJvdXRlci5wdXNoKCcvcmVzdGF1cmFudC9qb2JzL2NyZWF0ZScpO1xuICB9LFxuICAgICBkZWxldGVKb2IoaWQpXG4gICAgIHtcblxuICAgICAgICB2YXIgdG9rZW49bG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Rva2VuJyk7XG5cbiAgICAgICAgY29uc3QgaGVhZGVycyA9IHsgXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6J0JlYXJlciAnICsgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICBsZXQgZm9ybWRhdGE9IG5ldyBGb3JtRGF0YSgpO1xuICAgICBmb3JtZGF0YS5hcHBlbmQoJ2lkJyxpZCk7XG4gICAgIGF4aW9zKHttZXRob2Q6J3Bvc3QnLFxuICAgICAgICAgICB1cmw6Jy9hcGkvZGVsZXRlSm9iJyxcbiAgICAgICAgICAnaGVhZGVycyc6aGVhZGVycyxcbiAgICAgICAgICBkYXRhOmZvcm1kYXRhXG5cbiAgICAgICAgICB9KS50aGVuKHJlc3BvbnNlID0+IFxuICAgIHtcbiAgICAgICBsZXQgbGVuZ3RoPXRoaXMucmVzdGF1cmFudHMubGVuZ3RoO1xuICAgICAgbGV0IHJlcz0gdGhpcy5yZXN0YXVyYW50cztcbiAgICAgIGZvcihsZXQgaT0wO2k8bGVuZ3RoO2krKylcbiAgICAge1xuICAgICAgICBpZihyZXNbaV0uaWQ9PWlkKVxuICAgICAgICB7XG5cbiAgICAgICAgICAgdGhpcy5yZXN0YXVyYW50cy5zcGxpY2UoaSwxKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuICAgICB9XG5cblxuICAgIH0pXG4gICAgLmNhdGNoKGUgPT4ge1xuICAgICAgdGhpcy5lcnJvcnMucHVzaChlKVxuICAgIH0pXG4gICAgIH1cbiB9XG59XG48L3NjcmlwdD4iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFnZV93cmFwXCIgfSwgW1xuICAgIF92bS5fbSgwKSxcbiAgICBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcInJlc3RhdXJlbnRfZmlsdGVyXCIgfSxcbiAgICAgIFtcbiAgICAgICAgX2MoXG4gICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICB7XG4gICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidXR0b24gYnV0dG9uX21kIGJ1dHRvbi1jb2xvci1vXCIsXG4gICAgICAgICAgICBvbjogeyBjbGljazogX3ZtLmFkZCB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBbX3ZtLl92KFwiQWRkXCIpXVxuICAgICAgICApLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRhdGEtdGFibGVcIiwge1xuICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICBjb2x1bW5zOiBfdm0uY29sdW1ucyxcbiAgICAgICAgICAgIHVybDogXCIvYXBpL2FsbEpvYnNcIixcbiAgICAgICAgICAgIGhlYWRlcnM6IF92bS5oZWFkZXJcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICBdLFxuICAgICAgMVxuICAgIClcbiAgXSlcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFnZS10aXRsZVwiIH0sIFtcbiAgICAgIF9jKFwiaDJcIiwgW192bS5fdihcIlZlbmRvclwiKV0pXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IGZyb20gXCIuL0FsbEpvYnMudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTg4NzU4MjBjJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL0FsbEpvYnMudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9BbGxKb2JzLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuXG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qc1wiXG52YXIgY29tcG9uZW50ID0gbm9ybWFsaXplcihcbiAgc2NyaXB0LFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZmFsc2UsXG4gIG51bGwsXG4gIG51bGwsXG4gIG51bGxcbiAgXG4pXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiL3Zhci93d3cvaHRtbC9vcmRlYXp5L25vZGVfbW9kdWxlcy92dWUtaG90LXJlbG9hZC1hcGkvZGlzdC9pbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzg4NzU4MjBjJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzg4NzU4MjBjJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzg4NzU4MjBjJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9BbGxKb2JzLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD04ODc1ODIwYyZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCc4ODc1ODIwYycsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwicmVzb3VyY2VzL2pzL3ZpZXdzL1Jlc3RhdXJhbnRKb2JzL0FsbEpvYnMudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS00LTAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BbGxKb2JzLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS00LTAhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BbGxKb2JzLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BbGxKb2JzLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD04ODc1ODIwYyZcIiJdLCJzb3VyY2VSb290IjoiIn0=